﻿using System;

namespace P111
{
    public static class CastHelper
    {
        public static T ForcedCast<T>(Object obj)
        {
            return (T)Convert.ChangeType(obj, typeof(T));
        }
    }
}
