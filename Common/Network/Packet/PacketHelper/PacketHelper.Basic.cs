﻿using System;
using System.Collections.Generic;
using OpenTK;
using P111.GameObject.Building;
using P111.Network.Packet;
using P111.Network.Packet.Impl;
using P111.Player;

namespace P111.Server.AI.PacketHelper
{
    public static partial class PacketHelper
    {
        public static Vector2 GetUVCoord(User owner, User[] ulist)
        {
            if (owner == ulist[0])
                return Vector2.Zero;
            else if (owner == ulist[1])
                return Vector2.UnitX;
            else if (owner == ulist[2])
                return Vector2.UnitY;
            else
                throw new ArgumentException("User Not Found");
        }
        public static IEnumerable<PacketBase> SendInitPacket(User owner, Vector2 uvCoord)
        {
            Queue<PacketBase> outputQueue = new Queue<PacketBase>();

            if (uvCoord == Vector2.Zero)
            {
                outputQueue.Enqueue(new BuildingCreatePacket(owner, BuildingDataFactory.Castle, uvCoord / 3 * 2 + Vector2.Zero / 3));
                outputQueue.Enqueue(new BuildingCreatePacket(owner, BuildingDataFactory.Tower, uvCoord / 3 * 2 + Vector2.UnitX / 3));
                outputQueue.Enqueue(new BuildingCreatePacket(owner, BuildingDataFactory.Tower, uvCoord / 3 * 2 + Vector2.UnitY / 3));
            }
            else if (uvCoord == Vector2.UnitX)
            {
                outputQueue.Enqueue(new BuildingCreatePacket(owner, BuildingDataFactory.Tower, uvCoord / 3 * 2 + Vector2.Zero / 3));
                outputQueue.Enqueue(new BuildingCreatePacket(owner, BuildingDataFactory.Castle, uvCoord / 3 * 2 + Vector2.UnitX / 3));
                outputQueue.Enqueue(new BuildingCreatePacket(owner, BuildingDataFactory.Tower, uvCoord / 3 * 2 + Vector2.UnitY / 3));
            }
            else if (uvCoord == Vector2.UnitY)
            {
                outputQueue.Enqueue(new BuildingCreatePacket(owner, BuildingDataFactory.Tower, uvCoord / 3 * 2 + Vector2.Zero / 3));
                outputQueue.Enqueue(new BuildingCreatePacket(owner, BuildingDataFactory.Tower, uvCoord / 3 * 2 + Vector2.UnitX / 3));
                outputQueue.Enqueue(new BuildingCreatePacket(owner, BuildingDataFactory.Castle, uvCoord / 3 * 2 + Vector2.UnitY / 3));
            }

            return outputQueue;
        }
    }
}
