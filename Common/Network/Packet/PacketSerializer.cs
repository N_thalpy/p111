﻿using System;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace P111.Network.Packet
{
    public static class PacketSerializer
    {
        private static XmlSerializer xs;

        static PacketSerializer()
        {
            xs = new XmlSerializer(typeof(PacketBase), typeof(PacketBase).Assembly.GetTypes().Where(_ => _.IsSubclassOf(typeof(PacketBase))).ToArray());
        }

        public static Byte[] Serialize(PacketBase pb)
        {
            return xs.Serialize(pb).ToByteArray();
        }
        
        public static bool TryDeserialize<T>(Byte[] arr) where T : PacketBase
        {
            return xs.CanDeserialize<T>(Encoding.UTF8.GetString(arr));
        }

        public static T Deserialize<T>(Byte[] arr) where T : PacketBase
        {
            return xs.Deserialize<T>(Encoding.UTF8.GetString(arr));
        }
    }
}
