﻿using System;
using OpenTK;
using System.Collections.Generic;

namespace P111.Network.Packet.Impl
{
    [Serializable]
    [SentByClient]
    public class UnitCommandPacket : PacketBase
    {
        public enum CommandEnum
        {
            None,

            MoveToCenter,

            MoveToC1,
            MoveToC2,
            MoveToC3,

            MoveToT12,
            MoveToT21,
            MoveToT13,
            MoveToT31,
            MoveToT23,
            MoveToT32,

            End,
        }
        
        public int SquadHash;
        public CommandEnum Command;
        
        public UnitCommandPacket()
        {
        }
        public UnitCommandPacket(int squadHash, CommandEnum command)
        {
            SquadHash = squadHash;
            Command = command;
        }
        
        public static Vector2 CommandToPosition(CommandEnum command)
        {
            Vector2 aPos = Vector2.Zero;
            Vector2 bPos = Vector2.UnitX;
            Vector2 cPos = Vector2.UnitY;

            switch (command)
            {
                case CommandEnum.MoveToCenter:
                    return (aPos + bPos + cPos) / 3;

                case CommandEnum.MoveToC1:
                    return aPos;

                case CommandEnum.MoveToC2:
                    return bPos;

                case CommandEnum.MoveToC3:
                    return cPos;

                case CommandEnum.MoveToT12:
                    return (2 * aPos + bPos) / 3;

                case CommandEnum.MoveToT13:
                    return (2 * aPos + cPos) / 3;

                case CommandEnum.MoveToT21:
                    return (2 * bPos + aPos) / 3;

                case CommandEnum.MoveToT23:
                    return (2 * bPos + cPos) / 3;

                case CommandEnum.MoveToT31:
                    return (2 * cPos + aPos) / 3;

                case CommandEnum.MoveToT32:
                    return (2 * cPos + bPos) / 3;

                case CommandEnum.None:
                case CommandEnum.End:
                default:
                    throw new ArgumentException("Wrong Destination enum Value");
            }
        }
    }
}

