﻿using System;
using P111.Player;

namespace P111.Network.Packet.Impl
{
    [Serializable]
    [SentByServer]
    public class UnitKillPacket : PacketBase
    {
        public int UnitHashCode;
        
        public UnitKillPacket()
        {
        }
        public UnitKillPacket(int unitHashCode)
        {
            Sender = new User();
            UnitHashCode = unitHashCode;
        }
    }
}

