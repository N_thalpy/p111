﻿using System;
using P111.Player;

namespace P111.Network.Packet.Impl
{
    [Serializable]
    [SentByClient]
    public class StepPacket : PacketBase
    {
        public int Frame;
        public PacketBase[] Packets;
        
        public StepPacket()
        {
        }
        public StepPacket(int frame, PacketBase[] packetList)
        {
            Frame = frame;
            Packets = (PacketBase[])packetList.Clone();
        }
    }
}
