﻿using System;
using P111.Player;

namespace P111.Network.Packet.Impl
{
    [SentByServer]
    public class GameEndPacket : PacketBase
    {
        public enum PlayerState
        {
            Undefined,
            Win,
            Lose
        }

        public PlayerState State;
        public User Target;
        
        public GameEndPacket()
        {
        }
        public GameEndPacket(User u, PlayerState state)
        {
            Target = u;
            State = state;
        }
    }
}
