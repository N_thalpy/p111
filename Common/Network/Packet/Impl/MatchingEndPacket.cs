﻿using System;
using System.Collections.Generic;
using System.Linq;
using P111.Player;

namespace P111.Network.Packet.Impl
{
    [Serializable]
    [SentByServer]
    public class MatchingEndPacket : PacketBase
    {
        public User[] UserList;
        
        public MatchingEndPacket()
        {
        }
        public MatchingEndPacket(IEnumerable<User> userList)
        {
            UserList = userList.ToArray();
        }
    }
}

