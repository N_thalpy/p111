﻿using System;

namespace P111.Network.Packet.Impl
{
    [SentByServer]
    public class TimePacket : PacketBase
    {
        public float Remain;

        public TimePacket()
        {
        }
        public TimePacket(float remain)
        {
            Remain = remain;
        }
    }
}
