﻿using System;
using OpenTK;
using P111.Player;

namespace P111.Network.Packet.Impl
{
    [Serializable]
    [SentByServer]
    [SentByClient]
    public class UnitCreatePacket : PacketBase
    {
        public User Owner;
        public int UnitDataHash;
        public Vector2 UVCoord;
        public int TargetBuildingHash;
        public int UnitHashCode;
        public int UnitSquadCode;
        
        public UnitCreatePacket()
        {
        }
        public UnitCreatePacket(User owner, int unitDataHash, int targetBuildingHash, Vector2 uvCoord)
        {
            Sender = owner;
            Owner = owner;
            UnitDataHash = unitDataHash;
            TargetBuildingHash = targetBuildingHash;
            UVCoord = uvCoord;
            UnitHashCode = 0;
            UnitSquadCode = 0;
        }
    }
}

