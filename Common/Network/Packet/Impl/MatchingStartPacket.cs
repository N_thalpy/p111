﻿using System;
using P111.Player;

namespace P111.Network.Packet.Impl
{
    [Serializable]
    [SentByClient]
    public class MatchingStartPacket : PacketBase
    {
        public User User;
        
        public MatchingStartPacket()
        {
        }
        public MatchingStartPacket(User user)
        {
            Sender = new User();
            User = user;
        }
    }
}

