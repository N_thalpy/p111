﻿using P111.Player;
using System;

namespace P111.Network.Packet.Impl
{
    [Serializable]
    [SentByServer]
    public class BuildingStatusPacket : PacketBase
    {
        public User Owner;
        public int BuildingHashCode;
        public float HP;
        public int[] InsideHashCode;
        
        public BuildingStatusPacket()
        {
        }
        public BuildingStatusPacket(User owner, int hashCode, float hp, int[] insideHashCode)
        {
            Owner = owner;
            BuildingHashCode = hashCode;
            HP = hp;
            InsideHashCode = insideHashCode;
        }
    }
}

