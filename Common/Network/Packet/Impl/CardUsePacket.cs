﻿using OpenTK;
using System;
using P111.Player;
using P111.GameObject.Card;

namespace P111.Network.Packet.Impl
{
    [Serializable]
    [SentByClient]
    public class CardUsePacket : PacketBase
    {
        public CardType CardType;
        public Vector2 UVPosition;
        
        public CardUsePacket()
        {
        }
        public CardUsePacket(CardType cardType, Vector2 uvPosition)
        {
            Sender = new User();
            CardType = cardType;
            UVPosition = uvPosition;
        }
    }
}
