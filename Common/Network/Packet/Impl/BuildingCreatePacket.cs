﻿using System;
using OpenTK;
using P111.Player;
using P111.GameObject.Building;

namespace P111.Network.Packet.Impl
{
    [Serializable]
    [SentByClient] 
    [SentByServer]
    public class BuildingCreatePacket : PacketBase
    {
        public BuildingData BuildingData;
        public Vector2 UVCoord;
        public int BuildingHashCode;
        
        public BuildingCreatePacket()
        {
        }
        public BuildingCreatePacket(User owner, BuildingData buildingData, Vector2 uvCoord)
        {
            Sender = owner;
            BuildingData = buildingData;
            UVCoord = uvCoord;
            BuildingHashCode = -1;
        }
    }
}

