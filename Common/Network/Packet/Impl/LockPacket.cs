﻿using P111.Player;
using System;

namespace P111.Network.Packet.Impl
{
    [Serializable]
    [SentByServer]
    public class LockPacket : PacketBase
    {
        public static String Signature;

        public int Frame;
        public int Hash;
        public PacketBase[] Packets;
        
        public LockPacket()
        {
        }
        public LockPacket(int hash, int frame, PacketBase[] packetList)
        {
            Frame = frame;
            Hash = hash;
            
            Packets = (PacketBase[])packetList.Clone();
        }
    }
}
