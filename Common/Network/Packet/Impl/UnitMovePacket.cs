﻿using System;
using OpenTK;

namespace P111.Network.Packet.Impl
{
    [Serializable]
    [SentByServer]
    public class UnitMovePacket : PacketBase
    {
        public int UnitHash;
        public Vector2 UVCoord;
        public Vector2 UVDestination;
        public bool IsInsideBuilding;

        public UnitMovePacket()
        {
        }
        public UnitMovePacket(int unitHash, Vector2 uvDest, Vector2 uvCoord, bool isInsideBuilding)
        {
            UnitHash = unitHash;
            UVDestination = uvDest;
            UVCoord = uvCoord;
            IsInsideBuilding = isInsideBuilding;
        }
    }
}

