﻿using System;
using P111.Player;

namespace P111.Network.Packet.Impl
{
    [Serializable]
    [SentByServer]
    public class UnitHPPacket : PacketBase
    {
        public int UnitHashCode;
        public float HP;
        
        public UnitHPPacket()
        {
        }
        public UnitHPPacket(int hashCode, float hp)
        {
            UnitHashCode = hashCode;
            HP = hp;
        }
    }
}

