﻿using OpenTK;
using System;

namespace P111.Network
{
    public class CoordinateSystem
    {
        public static readonly CoordinateSystem Cartesian;
        public static readonly CoordinateSystem Triangular;

        public readonly Vector2 UnitX;
        public readonly Vector2 UnitY;
        public readonly Vector2 Origin;

        static CoordinateSystem()
        {
            Cartesian = new CoordinateSystem(Vector2.Zero, Vector2.UnitX, Vector2.UnitY);
            Triangular = new CoordinateSystem(Vector2.Zero, new Vector2(-0.5f, (float)Math.Sqrt(3) / 2), new Vector2(0.5f, (float)Math.Sqrt(3) / 2));
        }
        public CoordinateSystem(Vector2 origin, Vector2 unitX, Vector2 unitY)
        {
            this.Origin = origin;
            this.UnitX = unitX;
            this.UnitY = unitY;
        }

        public Vector2 ChangeSystem(Vector2 vec, CoordinateSystem system)
        {
            Matrix2 src = new Matrix2(
                UnitX.X, UnitY.X, 
                UnitX.Y, UnitY.Y);
            Matrix2 invdst = new Matrix2(
                system.UnitY.Y, -system.UnitY.X,
                -system.UnitX.Y, system.UnitX.X);
            Matrix2.Multiply(ref invdst, 1 / (system.UnitX.X * system.UnitY.Y - system.UnitX.Y * system.UnitY.X), out invdst);
            
            Matrix2 res = new Matrix2();
            Matrix2.Multiply(ref invdst, ref src, out res);

            Vector2 translated = new Vector2();
            Matrix2.Transform(ref res, ref vec, out translated);

            return translated;
        }
    }
}

