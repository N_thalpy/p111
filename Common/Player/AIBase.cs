﻿using P111.Network.Packet;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace P111.Player
{
    public abstract class AIBase
    {
        protected Queue<PacketBase> inputPacketQueue;
        protected Queue<PacketBase> outputPacketQueue;
        private IEnumerator innerStep;
        public User Owner;

        protected AIBase(User owner)
        {
            inputPacketQueue = new Queue<PacketBase>();
            outputPacketQueue = new Queue<PacketBase>();
            innerStep = InnerStep();

            Owner = owner;
        }

        public abstract IEnumerable<PacketBase> Init(IEnumerable<User> ulist);
        public abstract IEnumerator InnerStep();

        public void Step()
        {
            innerStep.MoveNext();
            inputPacketQueue.Clear();
        }

        public void AppendPacket(IEnumerable<PacketBase> packets)
        {
            lock (outputPacketQueue)
            {
                foreach (PacketBase p in packets)
                {
                    Debug.Assert(p.Sender == Owner);
                    outputPacketQueue.Enqueue(p);
                }
            }
        }
        public void AppendPacket(PacketBase p)
        {
            lock (outputPacketQueue)
            {
                Debug.Assert(p.Sender == Owner);
                outputPacketQueue.Enqueue(p);
            }
        }
        public IEnumerable<PacketBase> FlushPacket()
        {
            lock (outputPacketQueue)
            {
                IEnumerable<PacketBase> pb = outputPacketQueue.ToArray();
                outputPacketQueue.Clear();
                return pb;
            }
        }
    }
}
