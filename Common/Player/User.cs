﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using P111.Network.Packet;
using P111.Server.AI.PacketHelper;

namespace P111.Player
{
    [Serializable]
    public class User
    {
        // Singleton
        public static User Me;

        public int HashCode;
        public String Name;
        
        public bool IsDummy;
        [field: XmlIgnore]
        [field: NonSerialized]
        public AIBase AI;
        
        public User()
        {
        }
        public User(String name)
        {
            HashCode = Math.Abs((name + DateTime.Now).GetHashCode()) % 65535;
            Name = name;
        }

        public IEnumerable<PacketBase> Init(IEnumerable<User> ulist)
        {
            Queue<PacketBase> pbQueue = new Queue<PacketBase>();
            User[] userList = ulist.ToArray();
            foreach (PacketBase pb in PacketHelper.SendInitPacket(this, PacketHelper.GetUVCoord(this, userList)))
                pbQueue.Enqueue(pb);

            if (IsDummy == true)
                foreach (PacketBase pb in AI.Init(ulist))
                    pbQueue.Enqueue(pb);

            return pbQueue;
        }

        public override int GetHashCode()
        {
            return HashCode;
        }
        public override string ToString()
        {
            return string.Format("{0} ({1})", Name, HashCode);
        }

        public static bool operator ==(User lhs, User rhs)
        {
            if ((Object)lhs == null)
                return (Object)rhs == null;
            else if ((Object)rhs == null)
                return (Object)lhs == null;
            else
                return lhs.HashCode == rhs.HashCode;
        }
        public static bool operator !=(User lhs, User rhs)
        {
            return !(lhs == rhs);
        }
        public override bool Equals(object obj)
        {
            if (obj is User)
                return ((User)Convert.ChangeType(obj, typeof(User))).HashCode == this.HashCode;
            else
                return false;
        }
    }
}

