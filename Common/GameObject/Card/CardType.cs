﻿using System;
using System.Reflection;
using System.Linq;

namespace P111.GameObject.Card
{
    [Serializable]
    public class CardType
    {
        public static CardType ApprenticeKnight;
        public static CardType ApprenticeMage;
        public static CardType ApprenticeArcher;
        public static CardType Mage;
        public static CardType Knight;
        public static CardType Paladin;

        public static CardType Heist;

        [Flags]
        private enum Type
        {
            None = 0,

            UnitCard = 1 << 0,
            SpellCard = 1 << 1,
        }
        private Type type;
        public String Name;

        static CardType()
        {
            Heist = new CardType(Type.SpellCard);
            ApprenticeKnight = new CardType(Type.UnitCard);
            ApprenticeMage = new CardType(Type.UnitCard);
            ApprenticeArcher = new CardType(Type.UnitCard);
            Mage = new CardType(Type.UnitCard);
            Knight = new CardType(Type.UnitCard);
            Paladin = new CardType(Type.UnitCard);

            foreach (var e in typeof(CardType).GetFields(BindingFlags.Public | BindingFlags.Static)
                                              .Where(_ => _.GetValue(null).GetType() == typeof(CardType)))
            {
                (e.GetValue(null) as CardType).Name = e.Name;
            }
        }

        public CardType()
        {
        }
        /// <summary>
        /// Hidden .ctor
        /// </summary>
        private CardType(Type type)
        {
            this.type = type;
        }

        public bool IsUnit()
        {
            return (type & Type.UnitCard) != Type.None;
        }
        public bool IsSpell()
        {
            return (type & Type.SpellCard) != Type.None;
        }

        public static bool operator ==(CardType lhs, CardType rhs)
        {
            return lhs.Name == rhs.Name;
        }
        public static bool operator !=(CardType lhs, CardType rhs)
        {
            return lhs != rhs;
        }
        public override bool Equals(object obj)
        {
            if (obj is CardType)
                return (CardType)obj == this;
            return false;
        }
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}
