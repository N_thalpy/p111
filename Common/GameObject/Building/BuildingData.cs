﻿using System;
using OpenTK;

namespace P111.GameObject.Building
{
    public class BuildingData
    {
        public String TextureName;

        public Vector2 UVSize;

        public float MaxHP;
        public float Damage;
        public float AttackCycle;
        public float UVAttackRange;

        public BuildingData()
        {
        }
    }
}

