﻿using System;
using OpenTK;

namespace P111.GameObject.Building
{
    public static class BuildingDataFactory
    {
        public static BuildingData Castle = new BuildingData()
        {
            TextureName = "Castle",
            UVSize = Vector2.One * 0.14f,
            Damage = 40,
            AttackCycle = 50 / 100f,
            MaxHP = 1000,
            UVAttackRange = 4 * 0.05f,
        };
        public static BuildingData Tower = new BuildingData()
        {
            TextureName = "Tower",
            UVSize = Vector2.One * 0.1f,
            Damage = 40,
            AttackCycle = 50 / 100f,
            MaxHP = 1000,
            UVAttackRange = 4 * 0.05f,
        };
        public static BuildingData Tent = new BuildingData()
        {
            TextureName = "Tent",
            UVSize = Vector2.One * 0.1f,
            Damage = 0,
            AttackCycle = 1,
            MaxHP = 500,
            UVAttackRange = 0,
        };
    }
}

