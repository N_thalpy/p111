﻿using System;
using OpenTK;

#if CLIENT
using P111.Client.GameObject;
using Tri.And.Rendering;
#endif

namespace P111.GameObject.Unit
{
    [Serializable]
    public class UnitData
    {
#if CLIENT
        public UnitAnimation[] Animations;
        public Texture Icon;
#endif
        public int Hash;

        public float UVSpeed;
        public Vector2 UVSize;
        
        public float MaxHP;
        public float Damage;
        public float AttackCycle;
        public float UVAttackRange;
        public float UVAggroRange;

        public UnitData()
        {
        }
    }
}

