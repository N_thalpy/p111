﻿using OpenTK;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

#if CLIENT
using P111.Client.GameObject;
using Tri.And.Rendering;
#endif

namespace P111.GameObject.Unit
{
    public static class UnitDataFactory
    {
        public static Dictionary<int, UnitData> UnitDataDict;

        public static UnitData ApprenticeKnight;
        public static UnitData ApprenticeArcher;
        public static UnitData ApprenticeMage;
        public static UnitData Knight;
        public static UnitData Mage;
        public static UnitData Paladin;

        private static float CommonAggroRange;

        public static void Initialize()
        {
            CommonAggroRange = 0.1f;

            ApprenticeKnight = new UnitData()
            {
                UVSize = Vector2.One * 0.1f,
                UVSpeed = 4 * 0.05f,
                MaxHP = 250,
                Damage = 80,
                AttackCycle = 75 / 100f,
                UVAttackRange = 0.05f,
                UVAggroRange = CommonAggroRange,
            };
            ApprenticeArcher = new UnitData()
            {
                UVSize = Vector2.One * 0.1f,
                UVSpeed = 3 * 0.05f,
                MaxHP = 200,
                Damage = 60,
                AttackCycle = 60 / 100f,
                UVAttackRange = 3 * 0.05f,
                UVAggroRange = CommonAggroRange,
            };
            ApprenticeMage = new UnitData()
            {
                UVSize = Vector2.One * 0.1f,
                UVSpeed = 2 * 0.05f,
                MaxHP = 170,
                Damage = 50,
                AttackCycle = 50 / 100f,
                UVAttackRange = 4 * 0.05f,
                UVAggroRange = CommonAggroRange,
            };
            Knight = new UnitData()
            {
                UVSize = Vector2.One * 0.1f,
                UVSpeed = 4 * 0.05f,
                MaxHP = 300,
                Damage = 120,
                AttackCycle = 75 / 100f,
                UVAttackRange = 0.05f,
                UVAggroRange = CommonAggroRange,
            };
            Mage = new UnitData()
            {
                UVSize = Vector2.One * 0.1f,
                UVSpeed = 2 * 0.05f,
                MaxHP = 190,
                Damage = 65,
                AttackCycle = 50 / 100f,
                UVAttackRange = 4 * 0.05f,
                UVAggroRange = CommonAggroRange,
            };
            Paladin = new UnitData()
            {
                UVSize = Vector2.One * 0.1f,
                UVSpeed = 3 * 0.05f,
                MaxHP = 400,
                Damage = 50,
                AttackCycle = 70 / 100f,
                UVAttackRange = 0.05f,
                UVAggroRange = CommonAggroRange,
            };

            int hash = 0;
            UnitDataDict = new Dictionary<int, UnitData>();
            foreach (FieldInfo f in typeof(UnitDataFactory).GetFields(BindingFlags.Static | BindingFlags.Public)
                                                          .Where(_ => _.GetValue(null).GetType() == typeof(UnitData)))
            {
                UnitData u = f.GetValue(null) as UnitData;

                u.Hash = hash++;
                UnitDataDict.Add(u.Hash, u);               
            }


#if CLIENT
            ApprenticeKnight.Animations = new UnitAnimation[] { UnitAnimation.ApprenticeKnightTest };
            ApprenticeKnight.Icon = Texture.CreateFromBitmapPath("Resource/UnitIcon/ApprenticeKnight.png");
            
            ApprenticeArcher.Animations = new UnitAnimation[] { UnitAnimation.ApprenticeArcherTest };
            ApprenticeArcher.Icon = Texture.CreateFromBitmapPath("Resource/UnitIcon/ApprenticeArcher.png");

            ApprenticeMage.Animations = new UnitAnimation[] { UnitAnimation.ApprenticeMageTest };
            ApprenticeMage.Icon = Texture.CreateFromBitmapPath("Resource/UnitIcon/ApprenticeMage.png");
#endif
        }
    }
}

