﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace P111
{
    public static class XmlSerializerExtension
    {
        public static String Serialize(this XmlSerializer xs, object graph)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                xs.Serialize(ms, graph);
                ms.Position = 0;

                using (StreamReader sr = new StreamReader(ms))
                    return sr.ReadToEnd();
            }
        }
        public static bool CanDeserialize<T>(this XmlSerializer xs, String s)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    using (StreamWriter sw = new StreamWriter(ms))
                    {
                        sw.Write(s);
                        sw.Flush();

                        ms.Position = 0;

                        return xs.Deserialize(ms) is T;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static T Deserialize<T>(this XmlSerializer xs, String s)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                using (StreamWriter sw = new StreamWriter(ms))
                {
                    sw.Write(s);
                    sw.Flush();

                    ms.Position = 0;

                    return (T)xs.Deserialize(ms);
                }
            }
        }
    }
}

