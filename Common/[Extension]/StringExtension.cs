﻿using System;
using System.Text;

namespace P111
{
    public static class StringExtension
    {
        public static Byte[] ToByteArray(this String str)
        {
            return Encoding.UTF8.GetBytes(str);
        }
    }
}
