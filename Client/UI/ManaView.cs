using OpenTK;
using System;
using System.Drawing;
using Tri;
using Tri.And;
using Tri.And.Rendering;
using Tri.And.UI;

namespace P111.Client.UI
{
    public class ManaView : View
    {
        public readonly static int MaxMana;

        private float currentMana;
        public int CurrentMana
        {
            get
            {
                return (int)Math.Floor(currentMana);
            }
            set
            {
                currentMana = Math.Min(value, MaxMana);
            }
        }

        public float ManaIncreaseRate;
        public bool Buffed;

        private Sprite gemSprite;
        private Sprite manaBackgroundSprite;
        private Sprite manaEmptySprite;
        private Sprite manaBarSprite;

        static ManaView()
        {
            MaxMana = 10;
        }
        public ManaView()
            : base()
        {
            currentMana = 0;
            ManaIncreaseRate = 0;
            Buffed = false;

            gemSprite = new Sprite()
            {
                Texture = Texture.CreateFromBitmapPath("Resource/UI/Mana_bar_gem.png"),
                Size = new Vector2(1, 1 / 5f) * GameSystem.MainForm.Viewport.X,
                Depth = RenderingConst.UIFrontDepth,
            };
            manaBackgroundSprite = new Sprite()
            {
                Texture = Texture.CreateFromBitmapPath("Resource/UI/Mana_bar_background.png"),
                Size = new Vector2(1, 1 / 5f) * GameSystem.MainForm.Viewport.X,
                Depth = RenderingConst.UIBackDepth,
            };
            manaEmptySprite = new Sprite()
            {
                Texture = Texture.CreateFromBitmapPath("Resource/UI/Mana_bar_empty.png"),
                Size = new Vector2(1, 1 / 5f) * GameSystem.MainForm.Viewport.X,
                Depth = RenderingConst.UIBackDepth,
            };
            manaBarSprite = new Sprite()
            {
                Texture = Texture.CreateFromBitmapPath("Resource/UI/Mana_bar_full.png"),
                Size = new Vector2(1, 1 / 5f) * GameSystem.MainForm.Viewport.X,
                Depth = RenderingConst.UIBackDepth - 1,
            };

            Size = gemSprite.Size;
        }

        protected override void OnRender(ref Matrix4 mat)
        {
            gemSprite.Position = Position;
            gemSprite.Anchor = Anchor;
            manaBackgroundSprite.Position = Position;
            manaBackgroundSprite.Anchor = Anchor;
            manaEmptySprite.Position = Position;
            manaEmptySprite.Anchor = Anchor;
            manaBarSprite.Position = Position;
            manaBarSprite.Anchor = Anchor;
            manaBarSprite.Size = new Vector2(currentMana / MaxMana, 1 / 5f) * GameSystem.MainForm.Viewport.X;
            manaBarSprite.UV = new RectangleF(0, 0, currentMana / MaxMana, 1);

            manaBackgroundSprite.Render(ref mat);
            manaEmptySprite.Render(ref mat);
            manaBarSprite.Render(ref mat);
            gemSprite.Render(ref mat);
        }
        protected override void OnUpdate(Second deltaTime)
        {
            if (Buffed == true)
                currentMana = Math.Min(currentMana + deltaTime * ManaIncreaseRate * 1.5f, MaxMana);
            else
                currentMana = Math.Min(currentMana + deltaTime * ManaIncreaseRate, MaxMana);
        }
    }
}