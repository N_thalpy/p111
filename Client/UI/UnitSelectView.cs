﻿using OpenTK;
using P111.Client.GameLoop;
using P111.Client.GameObject;
using P111.Network;
using P111.Network.Packet;
using P111.Network.Packet.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using Tri.And.Rendering;
using Tri.And.SystemComponent;
using Tri.And.UI;

namespace P111.Client.UI
{
    public class UnitSelectView : View
    {
        private float margin;
        public Building TargetBuilding;

        private Sprite sp;
        private Sprite background;

        private Nullable<int> selectedIdx;

        public UnitSelectView(float width, float height, float margin)
            : base()
        {
            this.margin = margin;
            Size = new Vector2(width, height);
            selectedIdx = null;

            sp = new Sprite()
            {
                Size = Vector2.One * (width - margin * 3) / 2,
                Depth = RenderingConst.UIFrontDepth,
            };
            background = new Sprite()
            {
                Size = new Vector2(Size.X, Size.Y * 1.1f),
                Texture = Texture.CreateFromBitmapPath("Resource/UI/UnitSelect.png"),
                Depth = RenderingConst.UIBackDepth,
            };
        }
        
        public IEnumerable<PacketBase> CheckUnitMove(List<InputHelper.TouchData> touchList, List<Building> buildingList, List<Unit> unitList, CoordinateSystem displayCoordSystem)
        {
            float leftx = Position.X - Anchor.X * Size.X;
            float downy = Position.Y - Anchor.Y * Size.Y;
            float upy = Position.Y + (1 - Anchor.Y) * Size.Y;

            float blockSize = sp.Size.X;

            List<PacketBase> packets = new List<PacketBase>();

            if (TargetBuilding != null)
                TargetBuilding.Selected = true;

            foreach (InputHelper.TouchData td in touchList)
            {
                if (TargetBuilding != null && TargetBuilding.Owner == MainGameLoop.Instance.Me)
                {
                    if (td.Action == Android.Views.MotionEventActions.Down)
                    {
                        for (int idx = 0; idx < TargetBuilding.InsideUnitList.Count; idx++)
                        {
                            if (leftx + margin + (margin + blockSize) * (idx % 2) < td.Position.X &&
                                leftx + (margin + blockSize) + (margin + blockSize) * (idx % 2) > td.Position.X &&
                                upy - margin - (margin + blockSize) * (idx / 2) > td.Position.Y &&
                                upy - (margin + blockSize) - (margin + blockSize) * (idx / 2) < td.Position.Y)
                                selectedIdx = idx;
                        }
                    }
                    if (td.Action == Android.Views.MotionEventActions.Up)
                    {
                        if (selectedIdx != null && buildingList.Exists(_ => (_.Position - td.Position).Length < 30))
                        {
                            Building b = buildingList.Find(_ => (_.Position - td.Position).Length < 30);
                            Vector2 bpos = CoordinateSystem.Cartesian.ChangeSystem(b.Position - displayCoordSystem.Origin, displayCoordSystem);

                            List<UnitCommandPacket.CommandEnum> commandList = new List<UnitCommandPacket.CommandEnum>();
                            for (int idx = (int)UnitCommandPacket.CommandEnum.None + 1; idx < (int)UnitCommandPacket.CommandEnum.End; idx++)
                                commandList.Add((UnitCommandPacket.CommandEnum)idx);
                            commandList = commandList.OrderBy(_ => (UnitCommandPacket.CommandToPosition(_) - bpos).Length).ToList();

                            packets.Add(new UnitCommandPacket(TargetBuilding.InsideUnitList[selectedIdx.Value].HashCode, commandList.First()));
                            selectedIdx = null;
                        }
                    }
                }
            }

            if (touchList.Count == 0)
                selectedIdx = null;

            return packets;
        }
        protected override void OnRender(ref Matrix4 mat)
        {
            Vector2 rightUpper = Position - new Vector2(Size.X * Anchor.X, Size.Y * Anchor.Y);
            int idx = 0;

            background.Position = Position;
            background.Anchor = Anchor;
            background.Render(ref mat);

            if (TargetBuilding != null)
            {
                foreach (Unit unit in TargetBuilding.InsideUnitList)
                {
                    sp.Anchor = new Vector2(0, 0);
                    sp.Position = rightUpper + new Vector2(
                        margin + (sp.Size.X + margin) * (idx % 2),
                        margin + (sp.Size.X + margin) * (4 - idx / 2));

                    sp.Texture = unit.UnitData.Icon;
                    sp.Render(ref mat);
                    idx++;
                }
            }
        }
    }
}