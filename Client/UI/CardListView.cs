using Android.Views;
using OpenTK;
using P111.Client.GameObject;
using P111.Network.Packet;
using P111.Network.Packet.Impl;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Tri;
using Tri.And.SystemComponent;
using Tri.And.UI;
using View = Tri.And.UI.View;

namespace P111.Client.UI
{
    public class CardListView : View
    {
        private enum CardState
        {
            None,
            Idle,
            Clicked,
        }
        
        public bool IsCardClicked
        {
            get
            {
                return currentCardState != CardState.Idle;
            }
        }
        public Card CurrentCard
        {
            get
            {
                if (cardList.Count == 0 || currentCardState == CardState.Idle)
                    return null;
                return distinctCardList[cardIdx];
            }
        }

        private float dx;
        private bool isMoving;
        private Nullable<int> touchTraceIdx;
        private Nullable<float> touchTracePrevX;
        private float touchTotalDX;

        private Vector2 cardSize;

        private int cardIdx;
        private List<Card> cardList;
        private List<Card> distinctCardList;
        private CardState currentCardState;

        private Sprite sp;
        private BitmapLabel count;
        private Sprite descSprite;

        private int cardInRow;

        public CardListView(float xsize)
            : base()
        {
            cardInRow = 3;

            dx = 0f;
            touchTraceIdx = null;
            touchTracePrevX = null;
            touchTotalDX = 0;

            cardIdx = 0;
            cardList = new List<Card>();
            cardSize = new Vector2(1, 4 / 3f) * xsize / cardInRow;
            currentCardState = CardState.Idle;
            
            Position = Vector2.Zero;
            Anchor = Vector2.Zero;
            Size = new Vector2(xsize, 4 / 3f * xsize / cardInRow);

            sp = new Sprite()
            {
                Anchor = Vector2.Zero,
                Size = new Vector2(1, 4/3f) * xsize / cardInRow,
                Texture = null,
                Color = Color.White,
                Depth = RenderingConst.UIFrontDepth + 1,
            };

            descSprite = new Sprite()
            {
                Anchor = Vector2.Zero,
                Size = new Vector2(cardSize.X * cardInRow, cardSize.Y),
                IsVisible = false,
                Color = Color.White,
                Depth = RenderingConst.UIFrontDepth,
            };

            count = new BitmapLabel()
            {
                BitmapFont = FontManager.Arial,
                FontSize = 50,
                Color = Color.Black,
                Depth = RenderingConst.UIFrontDepth,
            };
        }
        
        public void AppendCard(Card c)
        {
            cardList.Add(c);
        }

        public IEnumerable<PacketBase> UseCard(Vector2 duv, Vector2 suv, Vector2 screenPos, List<Building> buildingList, List<Unit> unitList, ManaView mv)
        {
            PacketBase[] pb = null;
            if (CurrentCard != null && CurrentCard.IsValidUse(duv, suv, screenPos, buildingList, unitList) == true && mv.CurrentMana >= CurrentCard.ManaRequire)
            {
                pb = new PacketBase[]
                {
                    new CardUsePacket(CurrentCard.CardType, duv),
                };

                mv.CurrentMana -= CurrentCard.ManaRequire;

                currentCardState = CardState.Idle;
                cardList.Remove(distinctCardList[cardIdx]);
                cardIdx = Math.Max(0, cardIdx - 1);
            }

            if (pb == null)
                pb = new PacketBase[] { };
            return pb;
        }
        
        protected override void OnUpdate(Second deltaTime)
        {
            distinctCardList = cardList.Distinct().OrderBy(_ => _.CardType.Name).ToList();

            if (touchTraceIdx != null && touchTraceIdx >= InputHelper.TouchList.Count)
            {
                touchTraceIdx = null;
                touchTracePrevX = null;
            }

            foreach (InputHelper.TouchData td in InputHelper.TouchList)
            {
                if (td.Action == MotionEventActions.Down)
                {
                    if (0 <= td.Position.X && td.Position.X <= Size.X &&
                        0 <= td.Position.Y && td.Position.Y <= Size.Y &&
                        td.Index != -1)
                    {
                        touchTraceIdx = td.Index;
                        touchTotalDX = 0;
                        break;
                    }
                }
                else if (td.Action == MotionEventActions.Up)
                {
                    if (0 <= td.Position.X && td.Position.X <= Size.X &&
                        0 <= td.Position.Y && td.Position.Y <= Size.Y)
                    {
                        if (isMoving == false)
                        {
                            switch (currentCardState)
                            {
                                case CardState.Idle:
                                    cardIdx = (int)Math.Floor((td.Position.X - dx) / cardSize.X);
                                    currentCardState = CardState.Clicked;
                                    break;

                                case CardState.Clicked:
                                    if (cardIdx == (int)Math.Floor((td.Position.X - dx) / cardSize.X))
                                    {
                                        currentCardState = CardState.Idle;
                                    }
                                    else
                                    {
                                        cardIdx = (int)Math.Floor((td.Position.X - dx) / cardSize.X);
                                        currentCardState = CardState.Clicked;
                                    }
                                    break;
                            }

                            // Out-of-Index
                            if (cardIdx >= distinctCardList.Count)
                                currentCardState = CardState.Idle;
                        }
                    }
                }
            }
            if (InputHelper.TouchList.Count == 0)
                isMoving = false;

            if (touchTraceIdx != null)
            {
                if (touchTracePrevX != null)
                {
                    if (Math.Abs(InputHelper.TouchList[touchTraceIdx.Value].Position.X - touchTracePrevX.Value) > 10)
                        isMoving = true;

                    if (isMoving == true)
                    {
                        dx += InputHelper.TouchList[touchTraceIdx.Value].Position.X - touchTracePrevX.Value;
                        touchTotalDX += InputHelper.TouchList[touchTraceIdx.Value].Position.X - touchTracePrevX.Value;
                    }

                    dx = Math.Min(0, dx);
                    dx = Math.Max(dx, Math.Min(0, -(distinctCardList.Count - cardInRow) * cardSize.X));
                }
                touchTracePrevX = InputHelper.TouchList[touchTraceIdx.Value].Position.X;
            }
        }
        protected override void OnRender(ref Matrix4 mat)
        {
            float mostleftx = Position.X - Anchor.X * Size.X + dx;
            float mostlowery = Position.Y - Anchor.Y * Size.Y;
            descSprite.Position = new Vector2(Position.X - Anchor.X * Size.X, Position.Y - Anchor.Y * Size.Y);
            descSprite.IsVisible = false;

            distinctCardList = cardList.Distinct().OrderBy(_ => _.CardType.Name).ToList();

            for (int idx = 0; idx < distinctCardList.Count; idx++)
            {
                sp.Position = new Vector2(mostleftx + idx * sp.Size.X, mostlowery);
                sp.Texture = distinctCardList[idx].FrontTexture;
                count.Text = cardList.FindAll(_ => _ == distinctCardList[idx]).Count().ToString();

                count.Position = sp.Position + new Vector2(sp.Size.X, sp.Size.Y);
                count.Anchor = new Vector2(1, 1) * 1.1f;

                sp.Render(ref mat);
                count.Render(ref mat);
            }
            if (CurrentCard != null)
            {
                descSprite.IsVisible = true;
                descSprite.Texture = CurrentCard.DescriptTexture;
                descSprite.Render(ref mat);
            }
        }
    }
}