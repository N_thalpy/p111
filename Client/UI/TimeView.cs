using OpenTK;
using System;
using System.Drawing;
using Tri;
using Tri.And;
using Tri.And.Rendering;
using Tri.And.UI;

namespace P111.Client.UI
{
    public class TimeView : View
    {
        private static Second MaxTime;

        public Second Remain;
        private Sprite timeBackground;
        private Sprite timeSprite;

        static TimeView()
        {
            MaxTime = (Second)180;
        }
        public TimeView()
            : base()
        {
            Remain = MaxTime;
            timeBackground = new Sprite()
            {
                Size = new Vector2(1, 170 / 1500f) * GameSystem.MainForm.Viewport.X,
                Texture = Texture.CreateFromBitmapPath("Resource/UI/Time_background.png"),
                Depth = RenderingConst.UIFrontDepth + 1,
            };
            timeSprite = new Sprite()
            {
                Size = new Vector2(1, 170 / 1500f) * GameSystem.MainForm.Viewport.X,
                Texture = Texture.CreateFromBitmapPath("Resource/UI/Time_bar.png"),
                Depth = RenderingConst.UIFrontDepth,
            };

            Size = timeSprite.Size;
        }

        protected override void OnRender(ref Matrix4 mat)
        {
            timeBackground.Position = Position;
            timeBackground.Anchor = Anchor;
            timeSprite.Position = Position;
            timeSprite.Anchor = Anchor;
            timeSprite.Size = new Vector2(Remain / MaxTime, 170 / 1500f) * GameSystem.MainForm.Viewport.X;
            timeSprite.UV = new RectangleF(0, 0, Remain / MaxTime, 1);

            timeBackground.Render(ref mat);
            timeSprite.Render(ref mat);
        }
    }
}