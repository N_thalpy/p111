﻿namespace P111.Client
{
    public class RenderingConst
    {
        public const int MapDepth = 131;
        public const int BuildingDepth = 130;
        public const int SelectBackDepth = 128;
        public const int UnitDepth = 127;
        public const int SelectFrontDepth = 126;


        public const int UIBackDepth = 110;
        public const int UIFrontDepth = 100;

        public const int Backward = 1;
        public const int Forward = -1;
    }
}

