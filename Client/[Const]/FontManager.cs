using Tri.And.UI.BitmapFont;

namespace P111.Client
{
    public class FontManager
    {
        private static bool Initialized;
        public static BitmapFont Arial;

        static FontManager()
        {
            Initialized = false;
        }

        public static void Initialize()
        {
            if (Initialized == true)
                return;

            Arial = BitmapFont.Create("SystemResources/arial.fnt");
        }
    }
}