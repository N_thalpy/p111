﻿using Lidgren.Network;
using P111.Network;
using P111.Network.Packet;
using P111.Network.Packet.Impl;
using P111.Player;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Xml.Serialization;
using Tri.And;

namespace P111.Client.Network
{
    public class ServerConnector
    {
        // Singleton
        public static ServerConnector Instance
        {
            get;
            private set;
        }

        public static void Initialize(Func<Object> func)
        {
            ServerConnector.Instance = new ServerConnector(User.Me);
            new Thread((_) =>
            {
                Func<Object> callback = _ as Func<Object>;

                ServerConnector.Instance.Connect();
                GameSystem.RunOnMainThread<Object>(callback);
            }).Start(func);
        }

        public NetClient client;
        public bool IsConnected
        {
            get
            {
                return client.ConnectionStatus != NetConnectionStatus.Disconnected;
            }
        }

        private User me;

        private Queue<PacketBase> outgoingInGamePacketQueue;
        private Queue<PacketBase> incomingPacketQueue;

        private StepPacket backupStepPacket;

        public ServerConnector(User me)
        {
            this.me = me;

            NetPeerConfiguration config = new NetPeerConfiguration("P111");
            config.AutoFlushSendQueue = false;

            client = new NetClient(config);
            outgoingInGamePacketQueue = new Queue<PacketBase>();
            incomingPacketQueue = new Queue<PacketBase>();

            new Thread(() =>
            {
                while (true)
                {
                    ReadPacket();
                    Thread.Yield();
                }
            }).Start();
        }

        private void ReadPacket()
        {
            NetIncomingMessage im;
            lock (client)
                lock (incomingPacketQueue)
                    lock (outgoingInGamePacketQueue)
                    {
                        while ((im = client.ReadMessage()) != null)
                        {
                            // handle incoming message
                            switch (im.MessageType)
                            {
                                case NetIncomingMessageType.DebugMessage:
                                case NetIncomingMessageType.ErrorMessage:
                                case NetIncomingMessageType.WarningMessage:
                                case NetIncomingMessageType.VerboseDebugMessage:
                                    string text = im.ReadString();
                                    Console.WriteLine(text);
                                    break;

                                case NetIncomingMessageType.StatusChanged:
                                    NetConnectionStatus status = (NetConnectionStatus)im.ReadByte();
                                    string reason = im.ReadString();
                                    Console.WriteLine(status.ToString() + ": " + reason);
                                    break;

                                case NetIncomingMessageType.Data:
                                    int length = im.ReadInt32();
                                    Byte[] bytes = im.ReadBytes(length);

                                    if (PacketSerializer.TryDeserialize<LockPacket>(bytes))
                                    {
                                        StepPacket sp;
                                        LockPacket lp = PacketSerializer.Deserialize<LockPacket>(bytes);

                                        if (backupStepPacket != null && backupStepPacket.Frame >= lp.Frame)
                                        {
                                            if (backupStepPacket.Frame == lp.Frame)
                                            {
                                                sp = backupStepPacket;
                                                SendPacket(sp);
                                            }
                                        }
                                        else
                                        {
                                            foreach (PacketBase p in lp.Packets)
                                                incomingPacketQueue.Enqueue(p);

                                            sp = new StepPacket(lp.Frame, outgoingInGamePacketQueue.ToArray());
                                            outgoingInGamePacketQueue.Clear();
                                            sp.Sender = me;

                                            backupStepPacket = sp;
                                            SendPacket(sp);
                                        }
                                    }
                                    else if (PacketSerializer.TryDeserialize<MatchingEndPacket>(bytes))
                                    {
                                        incomingPacketQueue.Enqueue(PacketSerializer.Deserialize<MatchingEndPacket>(bytes));
                                    }
                                    else
                                        PacketSerializer.TryDeserialize<LockPacket>(bytes);

                                    break;

                                default:
                                    Console.WriteLine("Unhandled type: " + im.MessageType + " " + im.LengthBytes + " bytes");
                                    break;
                            }

                            client.Recycle(im);
                        }
                    }
        }

        public void Connect()
        {
            client.Start();
            XmlSerializer userXs = new XmlSerializer(typeof(User));
            NetOutgoingMessage hail = client.CreateMessage(userXs.Serialize(me));
            client.Connect(NetworkConstant.Host, NetworkConstant.Port, hail);

            while (client.ConnectionStatus == NetConnectionStatus.Disconnected)
                Thread.Yield();

        }

        public void EnqueueGamePacket(PacketBase pb)
        {
            new Thread(InnerEnqueueGamePacket).Start(new PacketBase[] { pb });
        }
        public void EnqueueGamePacket(IEnumerable<PacketBase> pb)
        {
            new Thread(InnerEnqueueGamePacket).Start(pb.ToArray());
        }
        private void InnerEnqueueGamePacket(Object arg)
        {
            PacketBase[] pbList = arg as PacketBase[];
            foreach (PacketBase pb in pbList)
                pb.Sender = me;

            lock (outgoingInGamePacketQueue)
                foreach (PacketBase pb in pbList)
                    outgoingInGamePacketQueue.Enqueue(pb);
        }
        public void SendPacket(PacketBase pb)
        {
            new Thread(InnerSendPacket).Start(pb);
        }
        private void InnerSendPacket(Object arg)
        {
            PacketBase pb = arg as PacketBase;
            pb.Sender = me;

            lock (client)
            {
                NetOutgoingMessage msg = client.CreateMessage();
                Byte[] arr = PacketSerializer.Serialize(pb);
                msg.Write(arr.Length);
                msg.Write(arr);

                client.SendMessage(msg, NetDeliveryMethod.ReliableOrdered);
                client.FlushSendQueue();
            }
        }

        public List<PacketBase> GetPacketList(bool clear)
        {
            List<PacketBase> rv;

            // Make sure that this code never blocks main thread
            bool locked = Monitor.TryEnter(incomingPacketQueue);
            if (locked == false)
                return new List<PacketBase>();
            else
            {
                rv = incomingPacketQueue.ToList();
                if (clear)
                    incomingPacketQueue.Clear();
                Monitor.Exit(incomingPacketQueue);
            }
            return rv;
        }
    }
}

