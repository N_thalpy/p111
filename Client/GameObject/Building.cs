﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics;
using P111.GameObject.Building;
using Tri;
using Tri.And.Rendering;
using Tri.And.UI;
using Tri.And.UI.BitmapFont;
using P111.Player;
using P111.Client.GameLoop;

namespace P111.Client.GameObject
{
    public class Building
    {
        public static BitmapFont Font;

        private readonly Sprite selectSprite;
        private readonly Sprite sp;
        private readonly BitmapLabel lb;

        public User Owner;
        public Vector2 Position;
        public BuildingData BuildingData;
        public int HashCode;

        public float HP;
        public bool Selected;

        public List<Unit> InsideUnitList;

        static Building()
        {
            Font = FontManager.Arial;
        }
        public Building(BuildingData bd, float scale)
        {
            Selected = false;
            sp = new Sprite()
            {
                Depth = RenderingConst.BuildingDepth,
                Size = bd.UVSize * scale,
            };
            Texture selectTexture = Texture.CreateFromBitmapPath("Resource/UI/Select.png");
            selectSprite = new Sprite()
            {
                Depth = RenderingConst.BuildingDepth,
                Texture = selectTexture,
                Anchor = new Vector2(0.5f, 0.5f),
                Size = Vector2.Normalize(selectTexture.Size) * 1.5f * bd.UVSize.X * scale,
            };
            lb = new BitmapLabel()
            {
                BitmapFont = Building.Font,
                Anchor = new Vector2(0.5f, 0),
                Text = String.Empty,
                FontSize = 16,
                Color = Color4.Black,
            };

            BuildingData = bd;
            HP = BuildingData.MaxHP;
            InsideUnitList = new List<Unit>();
        }

        public void Update(Second deltaTime)
        {
            Selected = false;
            sp.Position = Position;
            selectSprite.Position = Position - 0.4f * sp.Size.Y * Vector2.UnitY;
            lb.Position = sp.Position + sp.Size.Y * sp.Anchor.Y * Vector2.UnitY;
            lb.Text = String.Format("{0} / {1}", (int)HP, BuildingData.MaxHP);
        }
        public void Render(ref Matrix4 mat)
        {
            String color = String.Empty;
            if (Owner == MainGameLoop.Instance.Me)
                color = "Red";
            else if (Owner == MainGameLoop.Instance.Enemy1)
                color = "Blue";
            else if (Owner == MainGameLoop.Instance.Enemy2)
                color = "Green";
            else
                color = "Colorless";

            lb.Render(ref mat);

            if (Selected == true)
                selectSprite.Render(ref mat);
            sp.Texture = Texture.CreateFromBitmapPath(String.Format("Resource/Building/{0}.{1}.png", BuildingData.TextureName, color));
            sp.Render(ref mat);
        }
    }
}

