﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics;
using P111.GameObject.Unit;
using P111.Player;
using Tri;
using Tri.And.Rendering;
using Tri.And.UI;
using Tri.And.UI.BitmapFont;

namespace P111.Client.GameObject
{
    public class Unit
    {
        public static BitmapFont Font;
        public static Dictionary<String, Texture> TextureDic;

        public enum ColorEnum
        {
            Red,
            Green,
            Blue,
        }

        private readonly UnitAnimation anim;
        private readonly Texture icon;
        private readonly BitmapLabel lb;

        private float speed;

        public User Owner;
        public ColorEnum Color;
        public Vector2 Position;
        public Vector2 Destination;
        public UnitData UnitData;
        public bool IsInsideBuilding;
        public int HashCode;
        public int SquadCode;

        public float HP;

        static Unit()
        {
            TextureDic = new Dictionary<String, Texture>();
            Font = FontManager.Arial;
        }
        public Unit(UnitData ud, float scale)
        {
            UnitData = ud;
            IsInsideBuilding = true;

            anim = UnitData.Animations[0];
            icon = UnitData.Icon;
            anim.Scale = scale * ud.UVSize.X / 3;

            lb = new BitmapLabel()
            {
                BitmapFont = Unit.Font,
                Anchor = new Vector2(0.5f, 0),
                Text = String.Empty,
                FontSize = 14,
                Color = Color4.Black,
            };

            HP = UnitData.MaxHP;
            speed = UnitData.UVSpeed * scale;
        }

        public void Update(Second deltaTime)
        {
            if (deltaTime != Second.Zero)
            {
                Vector2 vel = (Destination - Position) / deltaTime;
                if (vel.Length < speed)
                    Position = Destination;
                else
                    Position += Vector2.Normalize(vel) * speed * deltaTime;
            }

            anim.Update(deltaTime);
        }
        public void Render(ref Matrix4 mat)
        {
            if (IsInsideBuilding == false)
            {
                anim.Position = Position;
                anim.Render(ref mat);
                
                lb.Position = anim.Position + anim.Size.Y * anim.Scale / 400 * 1.1f * Vector2.UnitY;
                lb.Text = String.Format("{0} / {1}", (int)HP, UnitData.MaxHP);
                lb.Render(ref mat);
            }
        }
    }
}

