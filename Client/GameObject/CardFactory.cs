﻿using OpenTK;
using P111.Client.GameLoop;
using P111.GameObject.Card;
using System.Collections.Generic;

namespace P111.Client.GameObject
{
    public static class CardFactory
    {
        private static bool All(Vector2 duv, Vector2 suv, Vector2 origPos, List<Building> buildingList, List<Unit> unitList)
        {
            return suv.X >= 0 && suv.Y >= 0 && (suv.X + suv.Y) <= 1;
        }
        private static bool Home(Vector2 duv, Vector2 suv, Vector2 origPos, List<Building> buildingList, List<Unit> unitList)
        {
            Building b = buildingList.Find(_ => (origPos - _.Position).Length < 30);
            return b != null && b.Owner == MainGameLoop.Instance.Me;
        }

        public static Card Heist = new Card(CardType.Heist, All, 2);
        public static Card ApprenticeKnight = new Card(CardType.ApprenticeKnight, Home, 3);
        public static Card ApprenticeMage = new Card(CardType.ApprenticeMage, Home, 3);
        public static Card ApprenticeArcher = new Card(CardType.ApprenticeArcher, Home, 3);
        public static Card Mage = new Card(CardType.Mage, Home, 3);
        public static Card Paladin = new Card(CardType.Paladin, Home, 3);
        public static Card Knight = new Card(CardType.Knight, Home, 3);
    }
}
