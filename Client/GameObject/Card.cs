using System;
using System.Collections.Generic;
using OpenTK;
using P111.GameObject.Card;
using Tri.And.Rendering;

namespace P111.Client.GameObject
{
    public class Card
    {
        public delegate bool ValidChecker(Vector2 duv, Vector2 suv, Vector2 screenPos, List<Building> buildingList, List<Unit> unitList);

        public static Dictionary<String, Texture> FrontTextureDic;
        public static Dictionary<String, Texture> DescriptTextureDic;

        public CardType CardType;
        public Texture FrontTexture;
        public Texture DescriptTexture;
        public ValidChecker IsValidUse;
        public int ManaRequire;
        
        static Card()
        {
            FrontTextureDic = new Dictionary<String, Texture>();
            DescriptTextureDic = new Dictionary<String, Texture>();
        }
        public Card(CardType cardType, ValidChecker isValidUse, int manaRequire)
        {
            String name = cardType.Name;
            if (FrontTextureDic.ContainsKey(name) == false)
            {
                FrontTextureDic.Add(name, Texture.CreateFromBitmapPath(String.Format("Resource/Card/{0}_Card.png", name)));
                DescriptTextureDic.Add(name, Texture.CreateFromBitmapPath(String.Format("Resource/Card/{0}_Desc.png", name))); 
            }

            CardType = cardType;
            FrontTexture = FrontTextureDic[name];
            DescriptTexture = DescriptTextureDic[name];

            IsValidUse = isValidUse;
            ManaRequire = manaRequire;
        }
    }
}