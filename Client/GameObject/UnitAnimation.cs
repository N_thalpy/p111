﻿using OpenTK;
using System.Collections.Generic;
using System.Linq;
using Tri;
using Tri.And.Rendering;
using Tri.And.UI;
using Tri.Mathematics;

namespace P111.Client.GameObject
{
    public class UnitAnimation
    {
        #region Non-Static
        public sealed class Component
        {
            public Vector2 Position
            {
                get
                {
                    if (parent == null)
                        return LocalPosition;
                    else
                        return parent.Position + LocalPosition.Rotate(parent.angle) * Owner.Scale;
                }
                set
                {
                    if (parent == null)
                        LocalPosition = value;
                    else
                        LocalPosition = ((value - parent.Position) / Owner.Scale).Rotate(-parent.angle);
                }
            }

            public Vector2 LocalPosition;
            public Texture Texture;
            public AnimatedDegreeCurve AngleCurve;
            public float DeltaDepth;

            public UnitAnimation Owner;

            private Degree angle;
            private Second timer;

            private Component parent;
            private List<Component> childs;
            private Sprite sp;

            public Component()
            {
                timer = Second.Zero;
                childs = new List<Component>();

                angle = Degree.Zero;
                timer = Second.Zero;
                sp = new Sprite();
            }
            public void AddChild(Component c)
            {
                childs.Add(c);
                c.parent = this;
            }
            public void SetOwner(UnitAnimation owner)
            {
                Owner = owner;
                foreach (Component c in childs)
                    c.SetOwner(owner);
            }
            public List<Component> GetComponents(List<Component> result)
            {
                result.Add(this);
                foreach (Component c in childs)
                    result = c.GetComponents(result);

                return result;
            }

            public void Update(Second deltaTime)
            {
                timer += deltaTime;
                if (AngleCurve != null)
                    angle = AngleCurve.GetValue(timer);
            }
            public void Render(ref Matrix4 mat)
            {
                sp.Position = Position;
                sp.Angle = angle;
                sp.Anchor = Vector2.One / 2;
                sp.Texture = Texture;
                sp.Size = sp.Texture.Size * Owner.Scale / 200;
                sp.Depth = RenderingConst.UnitDepth;

                sp.Render(ref mat);
            }
        }

        private Component root;
        
        public float Scale;
        public Vector2 Size
        {
            get
            {
                return root.Texture.Size;
            }
        }
        public Vector2 Position
        {
            get
            {
                return root.Position;
            }
            set
            {
                root.Position = value;
            }
        }

        public UnitAnimation(Component root)
        {
            this.root = root;
            this.root.SetOwner(this);
        }
        
        public void Update(Second deltaTime)
        {
            foreach (Component c in root.GetComponents(new List<Component>()))
                c.Update(deltaTime);
        }
        public void Render(ref Matrix4 mat)
        {
            foreach (Component c in root.GetComponents(new List<Component>()).OrderBy(_ => -_.DeltaDepth))
                c.Render(ref mat);                
        }
        #endregion

        #region Static
        public static UnitAnimation ApprenticeKnightTest;
        public static UnitAnimation ApprenticeMageTest;
        public static UnitAnimation ApprenticeArcherTest;
        public static UnitAnimation KnightTest;
        public static UnitAnimation WizardTest;

        static UnitAnimation()
        {
            InitializeApprenticeKnight();
            InitializeApprenticeMage();
            InitializeApprenticeArcher();
            InitializeKnight();
            InitializeWizard();
        }

        private static void InitializeApprenticeKnight()
        {
            Component body_upper = new Component()
            {
                LocalPosition = Vector2.Zero,
                AngleCurve = null,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeKnight/body_upper.png"),
                DeltaDepth = 0f,
            };

            Component body_lower = new Component()
            {
                LocalPosition = new Vector2(0, -0f),
                AngleCurve = null,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeKnight/body_lower.png"),
                DeltaDepth = RenderingConst.Backward * 0.1f,
            };
            body_upper.AddChild(body_lower);

            body_upper.AddChild(new Component()
            {
                LocalPosition = new Vector2(-0.05f, 0.7f),
                AngleCurve = null,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeKnight/head.png"),
                DeltaDepth = RenderingConst.Forward * 0.1f,
            });

            AnimatedDegreeCurve arm_left_sword_curve = new AnimatedDegreeCurve(InterpolationType.Linear);
            arm_left_sword_curve.AddKeyFrame((Second)0, (Degree)40);
            arm_left_sword_curve.AddKeyFrame((Second)0.5f, (Degree)60);
            arm_left_sword_curve.AddKeyFrame((Second)1, (Degree)40);
            body_upper.AddChild(new Component()
            {
                LocalPosition = new Vector2(0.25f, 0.5f),
                AngleCurve = arm_left_sword_curve,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeKnight/arm_left_sword.png"),
                DeltaDepth = RenderingConst.Backward * 0.1f,
            });

            AnimatedDegreeCurve arm_right_curve = new AnimatedDegreeCurve(InterpolationType.Linear);
            arm_right_curve.AddKeyFrame((Second)0, (Degree)(-40));
            arm_right_curve.AddKeyFrame((Second)0.5f, (Degree)(-60));
            arm_right_curve.AddKeyFrame((Second)1, (Degree)(-40));
            body_upper.AddChild(new Component()
            {
                LocalPosition = new Vector2(-0.3f, 0.5f),
                AngleCurve = arm_right_curve,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeKnight/arm_right.png"),
                DeltaDepth = RenderingConst.Forward * 0.1f,
            });

            AnimatedDegreeCurve leg_left_curve = new AnimatedDegreeCurve(InterpolationType.Linear);
            leg_left_curve.AddKeyFrame((Second)0, (Degree)(10));
            leg_left_curve.AddKeyFrame((Second)0.5f, (Degree)(20));
            leg_left_curve.AddKeyFrame((Second)1, (Degree)(10));
            body_lower.AddChild(new Component()
            {
                LocalPosition = new Vector2(0.1f, -0.2f),
                AngleCurve = leg_left_curve,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeKnight/leg_left.png"),
                DeltaDepth = RenderingConst.Backward * 0.2f,
            });

            AnimatedDegreeCurve leg_right_curve = new AnimatedDegreeCurve(InterpolationType.Linear);
            leg_right_curve.AddKeyFrame((Second)0, (Degree)(-10));
            leg_right_curve.AddKeyFrame((Second)0.5f, (Degree)(-20));
            leg_right_curve.AddKeyFrame((Second)1, (Degree)(-10));
            body_lower.AddChild(new Component()
            {
                LocalPosition = new Vector2(-0.1f, -0.2f),
                AngleCurve = leg_right_curve,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeKnight/leg_right.png"),
                DeltaDepth = RenderingConst.Backward * 0.2f,
            });

            ApprenticeKnightTest = new UnitAnimation(body_upper);
        }
        private static void InitializeApprenticeMage()
        {
            Component body_upper = new Component()
            {
                LocalPosition = Vector2.Zero,
                AngleCurve = null,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeMage/body_upper.png"),
                DeltaDepth = 0f,
            };
            Component body_lower = new Component()
            {
                LocalPosition = new Vector2(0, -0f),
                AngleCurve = null,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeMage/body_lower.png"),
                DeltaDepth = RenderingConst.Backward * 0.1f,
            };

            body_upper.AddChild(body_lower);

            body_upper.AddChild(new Component()
            {
                LocalPosition = new Vector2(-0.05f, 0.7f),
                AngleCurve = null,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeMage/head.png"),
                DeltaDepth = RenderingConst.Forward * 0.1f,
            });
            AnimatedDegreeCurve arm_left_wand_curve = new AnimatedDegreeCurve(InterpolationType.Linear);
            arm_left_wand_curve.AddKeyFrame((Second)0, (Degree)10);
            arm_left_wand_curve.AddKeyFrame((Second)0.5f, (Degree)20);
            arm_left_wand_curve.AddKeyFrame((Second)1, (Degree)10);
            body_upper.AddChild(new Component()
            {
                LocalPosition = new Vector2(0.25f, 0.5f),
                AngleCurve = arm_left_wand_curve,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeMage/arm_left_wand.png"),
                DeltaDepth = RenderingConst.Backward * 0.3f,
            });
            AnimatedDegreeCurve arm_right_curve = new AnimatedDegreeCurve(InterpolationType.Linear);
            arm_right_curve.AddKeyFrame((Second)0, (Degree)(-0));
            arm_right_curve.AddKeyFrame((Second)0.5f, (Degree)(-10));
            arm_right_curve.AddKeyFrame((Second)1, (Degree)(-0));
            body_upper.AddChild(new Component()
            {
                LocalPosition = new Vector2(-0.35f, 0.5f),
                AngleCurve = arm_right_curve,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeMage/arm_right.png"),
                DeltaDepth = RenderingConst.Forward * 0.1f,
            });
            AnimatedDegreeCurve leg_left_curve = new AnimatedDegreeCurve(InterpolationType.Linear);
            leg_left_curve.AddKeyFrame((Second)0, (Degree)(5));
            leg_left_curve.AddKeyFrame((Second)0.5f, (Degree)(10));
            leg_left_curve.AddKeyFrame((Second)1, (Degree)(5));
            body_lower.AddChild(new Component()
            {
                LocalPosition = new Vector2(0.1f, -0.2f),
                AngleCurve = leg_left_curve,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeMage/leg_left.png"),
                DeltaDepth = RenderingConst.Backward * 0.2f,
            });
            AnimatedDegreeCurve leg_right_curve = new AnimatedDegreeCurve(InterpolationType.Linear);
            leg_right_curve.AddKeyFrame((Second)0, (Degree)(-5));
            leg_right_curve.AddKeyFrame((Second)0.5f, (Degree)(-10));
            leg_right_curve.AddKeyFrame((Second)1, (Degree)(-5));
            body_lower.AddChild(new Component()
            {
                LocalPosition = new Vector2(-0.1f, -0.2f),
                AngleCurve = leg_right_curve,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeMage/leg_right.png"),
                DeltaDepth = RenderingConst.Backward * 0.2f,
            });

            ApprenticeMageTest = new UnitAnimation(body_upper);
        }
        private static void InitializeApprenticeArcher()
        {
            Component body_upper = new Component()
            {
                LocalPosition = Vector2.Zero,
                AngleCurve = null,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeArcher/body_upper.png"),
                DeltaDepth = 0.0f,
            };
            Component body_lower = new Component()
            {
                LocalPosition = new Vector2(0, -0f),
                AngleCurve = null,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeArcher/body_lower.png"),
                DeltaDepth = RenderingConst.Backward * 0.1f,
            };
            body_upper.AddChild(body_lower);

            body_upper.AddChild(new Component()
            {
                LocalPosition = new Vector2(-0.05f, 0.7f),
                AngleCurve = null,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeArcher/head.png"),
                DeltaDepth = RenderingConst.Backward * 0.1f,
            });
            AnimatedDegreeCurve arm_left_bow_curve = new AnimatedDegreeCurve(InterpolationType.Linear);
            arm_left_bow_curve.AddKeyFrame((Second)0, (Degree)10);
            arm_left_bow_curve.AddKeyFrame((Second)0.5f, (Degree)20);
            arm_left_bow_curve.AddKeyFrame((Second)1, (Degree)10);
            body_upper.AddChild(new Component()
            {
                LocalPosition = new Vector2(0.25f, 0.5f),
                AngleCurve = arm_left_bow_curve,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeArcher/arm_left_bow.png"),
                DeltaDepth = RenderingConst.Backward * 0.3f,
            });
            AnimatedDegreeCurve arm_right_curve = new AnimatedDegreeCurve(InterpolationType.Linear);
            arm_right_curve.AddKeyFrame((Second)0, (Degree)(-0));
            arm_right_curve.AddKeyFrame((Second)0.5f, (Degree)(-10));
            arm_right_curve.AddKeyFrame((Second)1, (Degree)(-0));
            body_upper.AddChild(new Component()
            {
                LocalPosition = new Vector2(-0.3f, 0.5f),
                AngleCurve = arm_right_curve,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeArcher/arm_right.png"),
                DeltaDepth = RenderingConst.Forward * 0.1f,
            });
            AnimatedDegreeCurve leg_left_curve = new AnimatedDegreeCurve(InterpolationType.Linear);
            leg_left_curve.AddKeyFrame((Second)0, (Degree)(5));
            leg_left_curve.AddKeyFrame((Second)0.5f, (Degree)(10));
            leg_left_curve.AddKeyFrame((Second)1, (Degree)(5));
            body_lower.AddChild(new Component()
            {
                LocalPosition = new Vector2(0.1f, -0.2f),
                AngleCurve = leg_left_curve,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeArcher/leg_left.png"),
                DeltaDepth = RenderingConst.Backward * 0.2f,
            });
            AnimatedDegreeCurve leg_right_curve = new AnimatedDegreeCurve(InterpolationType.Linear);
            leg_right_curve.AddKeyFrame((Second)0, (Degree)(-5));
            leg_right_curve.AddKeyFrame((Second)0.5f, (Degree)(-10));
            leg_right_curve.AddKeyFrame((Second)1, (Degree)(-5));
            body_lower.AddChild(new Component()
            {
                LocalPosition = new Vector2(-0.1f, -0.2f),
                AngleCurve = leg_right_curve,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/ApprenticeArcher/leg_right.png"),
                DeltaDepth = RenderingConst.Backward * 0.2f,
            });
            ApprenticeArcherTest = new UnitAnimation(body_upper);
        }
        private static void InitializeKnight()
        {
            Component body_upper = new Component()
            {
                LocalPosition = Vector2.Zero,
                AngleCurve = null,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/Knight/body_upper.png"),
                DeltaDepth = 0f,
            };

            Component body_lower = new Component()
            {
                LocalPosition = new Vector2(0, -0f),
                AngleCurve = null,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/Knight/body_lower.png"),
                DeltaDepth = RenderingConst.Backward * 0.1f,
            };
            body_upper.AddChild(body_lower);

            body_upper.AddChild(new Component()
            {
                LocalPosition = new Vector2(-0.15f, 0.7f),
                AngleCurve = null,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/Knight/head.png"),
                DeltaDepth = RenderingConst.Forward * 0.1f,
            });

            AnimatedDegreeCurve arm_left_sword_curve = new AnimatedDegreeCurve(InterpolationType.Linear);
            arm_left_sword_curve.AddKeyFrame((Second)0, (Degree)30);
            arm_left_sword_curve.AddKeyFrame((Second)0.5f, (Degree)40);
            arm_left_sword_curve.AddKeyFrame((Second)1, (Degree)30);
            body_upper.AddChild(new Component()
            {
                LocalPosition = new Vector2(0.1f, 0.5f),
                AngleCurve = arm_left_sword_curve,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/Knight/arm_left_sword.png"),
                DeltaDepth = RenderingConst.Backward * 0.1f,
            });

            AnimatedDegreeCurve arm_right_curve = new AnimatedDegreeCurve(InterpolationType.Linear);
            arm_right_curve.AddKeyFrame((Second)0, (Degree)(-10));
            arm_right_curve.AddKeyFrame((Second)0.5f, (Degree)(-20));
            arm_right_curve.AddKeyFrame((Second)1, (Degree)(-10));
            body_upper.AddChild(new Component()
            {
                LocalPosition = new Vector2(-0.35f, 0.5f),
                AngleCurve = arm_right_curve,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/Knight/arm_right.png"),
                DeltaDepth = RenderingConst.Forward * 0.1f,
            });

            AnimatedDegreeCurve leg_left_curve = new AnimatedDegreeCurve(InterpolationType.Linear);
            leg_left_curve.AddKeyFrame((Second)0, (Degree)(5));
            leg_left_curve.AddKeyFrame((Second)0.5f, (Degree)(10));
            leg_left_curve.AddKeyFrame((Second)1, (Degree)(5));
            body_lower.AddChild(new Component()
            {
                LocalPosition = new Vector2(0.1f, -0.2f),
                AngleCurve = leg_left_curve,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/Knight/leg_left.png"),
                DeltaDepth = RenderingConst.Backward * 0.2f,
            });

            AnimatedDegreeCurve leg_right_curve = new AnimatedDegreeCurve(InterpolationType.Linear);
            leg_right_curve.AddKeyFrame((Second)0, (Degree)(-5));
            leg_right_curve.AddKeyFrame((Second)0.5f, (Degree)(-10));
            leg_right_curve.AddKeyFrame((Second)1, (Degree)(-5));
            body_lower.AddChild(new Component()
            {
                LocalPosition = new Vector2(-0.1f, -0.2f),
                AngleCurve = leg_right_curve,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/Knight/leg_right.png"),
                DeltaDepth = RenderingConst.Backward * 0.2f,
            });

            KnightTest = new UnitAnimation(body_upper);
        }
        private static void InitializeWizard()
        {
            Component body_upper = new Component()
            {
                LocalPosition = Vector2.Zero,
                AngleCurve = null,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/Wizard/body_upper.png"),
                DeltaDepth = 0f,
            };
            Component body_lower = new Component()
            {
                LocalPosition = new Vector2(0, -0f),
                AngleCurve = null,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/Wizard/body_lower.png"),
                DeltaDepth = RenderingConst.Backward * 0.1f,
            };

            body_upper.AddChild(body_lower);

            body_upper.AddChild(new Component()
            {
                LocalPosition = new Vector2(-0.05f, 0.7f),
                AngleCurve = null,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/Wizard/head.png"),
                DeltaDepth = RenderingConst.Forward * 0.1f,
            });

            body_upper.AddChild(new Component()
            {
                LocalPosition = new Vector2(-0.05f, 0.8f),
                AngleCurve = null,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/Wizard/hat.png"),
                DeltaDepth = RenderingConst.Backward * 0.1f,
            });

            AnimatedDegreeCurve arm_left_wand_curve = new AnimatedDegreeCurve(InterpolationType.Linear);
            arm_left_wand_curve.AddKeyFrame((Second)0, (Degree)0);
            arm_left_wand_curve.AddKeyFrame((Second)0.5f, (Degree)10);
            arm_left_wand_curve.AddKeyFrame((Second)1, (Degree)0);

            body_upper.AddChild(new Component()
            {
                LocalPosition = new Vector2(0.15f, 0.5f),
                AngleCurve = arm_left_wand_curve,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/Wizard/arm_left_wand.png"),
                DeltaDepth = RenderingConst.Backward * 0.1f,
            });
            AnimatedDegreeCurve arm_right_curve = new AnimatedDegreeCurve(InterpolationType.Linear);
            arm_right_curve.AddKeyFrame((Second)0, (Degree)(0));
            arm_right_curve.AddKeyFrame((Second)0.5f, (Degree)(-10));
            arm_right_curve.AddKeyFrame((Second)1, (Degree)(0));
            body_upper.AddChild(new Component()
            {
                LocalPosition = new Vector2(-0.4f, 0.5f),
                AngleCurve = arm_right_curve,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/Wizard/arm_right.png"),
                DeltaDepth = RenderingConst.Backward * 0.1f,
            });
            body_lower.AddChild(new Component()
            {
                LocalPosition = new Vector2(0.1f, -0.1f),
                AngleCurve = null,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/Wizard/leg_left.png"),
                DeltaDepth = RenderingConst.Backward * 0.2f,
            });
            body_lower.AddChild(new Component()
            {
                LocalPosition = new Vector2(-0f, -0.1f),
                AngleCurve = null,
                Texture = Texture.CreateFromBitmapPath("Resource/Unit/Wizard/leg_right.png"),
                DeltaDepth = RenderingConst.Backward * 0.2f,
            });

            WizardTest = new UnitAnimation(body_upper);
        }
        #endregion
    }
}
