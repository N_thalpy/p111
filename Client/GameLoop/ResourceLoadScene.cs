﻿using System;
using System.Collections.Generic;
using System.Threading;
using OpenTK;
using OpenTK.Graphics;
using Tri;
using Tri.And;
using Tri.And.GameLoop;
using Tri.And.Rendering;
using Tri.And.Rendering.Camera;
using Tri.And.UI;

namespace P111.Client.GameLoop
{
    public class ResourceLoadScene : GameLoopBase
    {
        private BitmapLabel progressLabel;
        private OrthographicCamera ocam;

        private GameLoopBase nextLoop;
        private List<String> texList;
        private Object progressLock;
        private int progress;
        private int totalProgress;

        public ResourceLoadScene(List<String> texList, GameLoopBase next)
            : base("_ResourceLoadScene")
        {
            nextLoop = next;
            this.texList = texList;

            progressLock = new Object();
            progress = 0;
            totalProgress = texList.Count;
         }

        protected override void OnInitialize()
        {
            base.OnInitialize();

            GameSystem.LogSystem.WriteLine("ResourceLoadScene Entered");
            new Thread(() =>
            {
                LoadResource();
            }).Start();

            progressLabel = new BitmapLabel()
            {
                BitmapFont = FontManager.Arial,
                FontSize = 25,
                Text = String.Empty,
                Position = GameSystem.MainForm.Viewport / 2,
                Anchor = Vector2.One / 2,
                Color = Color4.Black,
            };
            ocam = new OrthographicCamera(GameSystem.MainForm.Viewport, GameSystem.MainForm.Viewport / 2);
        }
        public void LoadResource()
        {
            foreach (String path in texList)
            {
                Texture.CreateFromBitmapPath(path);
                lock (progressLock)
                    progress++;
            }
        }
        public override void Update(Second deltaTime)
        {
            bool locked = Monitor.TryEnter(progressLock);
            if (locked)
            {
                if (progress == totalProgress)
                {
                    GameSystem.RunGameLoop(nextLoop);
                    GameSystem.LogSystem.WriteLine("ResourceLoadScene Exited");
                }
                progressLabel.Text = String.Format("{0}/{1} Loaded", progress, totalProgress);

                Monitor.Exit(progressLock);
            }
        }
        public override void Render(Second deltaTime)
        {
            progressLabel.Render(ref ocam.Matrix);
        }
    }
}
