﻿using OpenTK;
using P111.Client.GameObject;
using P111.Client.Network;
using P111.Client.UI;
using P111.GameObject.Unit;
using P111.Network;
using P111.Network.Packet;
using P111.Network.Packet.Impl;
using P111.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using Tri;
using Tri.And;
using Tri.And.GameLoop;
using Tri.And.Rendering;
using Tri.And.Rendering.Camera;
using Tri.And.SystemComponent;
using Tri.And.UI;
using Tri.Mathematics;

namespace P111.Client.GameLoop
{
    public class MainGameLoop : GameLoopBase
    {
        public static MainGameLoop Instance;

        OrthographicCamera ocam;

        List<User> userList;
        List<Unit> unitList;
        List<Card> cardList;

        CardListView cardListView;
        ManaView manaView;
        TimeView timeView;
        UnitSelectView unitSelectView;

        List<Building> buildingList;
        public User Me, Enemy1, Enemy2;

        CoordinateSystem displayCoordSystem;
        CoordinateSystem serverCoordSystem;

        ServerConnector serverConnector;

        Dictionary<User, Vector2> userCastlePosDic;
        Vector2 centerPos = new Vector2(1, 1) / 3;
        Dictionary<User, Unit.ColorEnum> userColorDic;

        Sprite map;
        Sprite result;

        bool Finished;

        List<Unit> clickedUnit;
        Texture selectTexture;

        public MainGameLoop(IEnumerable<User> ulist, List<Card> clist)
            : base("_TestGameLoop")
        {
            Instance = this;

            unitList = new List<Unit>();
            buildingList = new List<Building>();

            ocam = new OrthographicCamera(GameSystem.MainForm.Viewport, GameSystem.MainForm.Viewport / 2);

            if (User.Me == null)
            {
                Me = new User("Me");
                User.Me = Me;
            }
            Me = User.Me;

            userList = ulist.ToList();
            clickedUnit = new List<Unit>();
            cardList = clist;
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            serverConnector = ServerConnector.Instance;

            #region User Initializing
            if (Me == userList[0])
            {
                Enemy1 = userList[1];
                Enemy2 = userList[2];
            }
            else if (Me == userList[1])
            {
                Enemy1 = userList[0];
                Enemy2 = userList[2];
            }
            else if (Me == userList[2])
            {
                Enemy1 = userList[1];
                Enemy2 = userList[0];
            }
            else
                throw new ArgumentException("User not found");
            #endregion

            cardListView = new CardListView(GameSystem.MainForm.Viewport.X);
            foreach (Card c in cardList)
                cardListView.AppendCard(c);

            manaView = new ManaView()
            {
                ManaIncreaseRate = 0.75f,
                Anchor = cardListView.Anchor,
                Position = cardListView.Position + Vector2.UnitY * cardListView.Size.Y,
            };

            timeView = new TimeView()
            {
                Anchor = new Vector2(0, 1),
                Position = new Vector2(0, GameSystem.MainForm.Viewport.Y),
            };

            float blockSize = timeView.Size.Y * 0.75f;
            float unitSelectMargin = blockSize / 10;
            unitSelectView = new UnitSelectView(blockSize * 2 + unitSelectMargin * 3, blockSize * 5 + unitSelectMargin * 6, unitSelectMargin)
            {
                Anchor = new Vector2(1, 0),
                Position = new Vector2(GameSystem.MainForm.Viewport.X, manaView.Position.Y + manaView.Size.Y),
            };

            result = new Sprite()
            {
                Position = GameSystem.MainForm.Viewport / 2,
                Depth = RenderingConst.UIFrontDepth,
            };

            #region User Castle Position Initializing / Coord System Initializing
            userCastlePosDic = new Dictionary<User, Vector2>();
            float xsize = GameSystem.MainForm.Viewport.X;
            float ysize = GameSystem.MainForm.Viewport.Y;
            float castleMargin = 120;

            Vector2 center = new Vector2(
                xsize / 2,
                ysize - ((ysize - (manaView.Size.Y + timeView.Size.Y + cardListView.Size.Y)) / 2 + timeView.Size.Y));
            float halfLength = xsize / 2 - castleMargin;

            userCastlePosDic.Add(userList[1], new Vector2(center.X - halfLength, center.Y + 1.5f * Mathf.Sqrt(3) / 3 * halfLength));
            userCastlePosDic.Add(userList[2], new Vector2(center.X + halfLength, center.Y + 1.5f * Mathf.Sqrt(3) / 3 * halfLength));
            userCastlePosDic.Add(userList[0], new Vector2(center.X, center.Y - 1.5f * Mathf.Sqrt(3) / 3 * halfLength));

            displayCoordSystem = new CoordinateSystem(
                userCastlePosDic[Me],
                userCastlePosDic[Enemy1] - userCastlePosDic[Me],
                userCastlePosDic[Enemy2] - userCastlePosDic[Me]);
            serverCoordSystem = new CoordinateSystem(
                userCastlePosDic[userList[0]],
                userCastlePosDic[userList[1]] - userCastlePosDic[userList[0]],
                userCastlePosDic[userList[2]] - userCastlePosDic[userList[0]]);
            #endregion                                   

            userColorDic = new Dictionary<User, Unit.ColorEnum>();
            userColorDic.Add(Me, Unit.ColorEnum.Red);
            userColorDic.Add(Enemy1, Unit.ColorEnum.Blue);
            userColorDic.Add(Enemy2, Unit.ColorEnum.Green);

            #region Texture Loading
            selectTexture = Texture.CreateFromBitmapPath("Resource/UI/Select.png");
            map = new Sprite()
            {
                Position = serverCoordSystem.ChangeSystem(centerPos, CoordinateSystem.Cartesian) + serverCoordSystem.Origin,
                Anchor = new Vector2(0.5f, 0.52f), 
                Size = Vector2.One * serverCoordSystem.UnitX.Length * 1.5f,
                Texture = Texture.CreateFromBitmapPath("Resource/UI/Map.png"),
                Depth = RenderingConst.MapDepth,
            };
            #endregion
        }

        public override void Update(Second deltaTime)
        {
            if (Finished == true)
                return;

            #region Receiving packet
            foreach (PacketBase pb in serverConnector.GetPacketList(true))
            {
                #region UnitCreatePacket
                if (pb is UnitCreatePacket)
                {
                    UnitCreatePacket ucp = (UnitCreatePacket)pb;
                    Vector2 pos = displayCoordSystem.ChangeSystem(ucp.UVCoord, CoordinateSystem.Cartesian) + displayCoordSystem.Origin;
                    Unit.ColorEnum color = userColorDic[ucp.Owner];

                    Unit u = new Unit(UnitDataFactory.UnitDataDict[ucp.UnitDataHash], serverCoordSystem.UnitX.Length);
                    u.Owner = ucp.Owner;
                    u.Position = pos;
                    u.Destination = pos;
                    u.Color = color;
                    u.HashCode = ucp.UnitHashCode;
                    u.SquadCode = ucp.UnitSquadCode;

                    unitList.Add(u);
                }
                #endregion
                #region UnitMovePacket
                else if (pb is UnitMovePacket)
                {
                    UnitMovePacket ump = (UnitMovePacket)pb;
                    foreach (Unit ub in unitList)
                        if (ub.HashCode == ump.UnitHash)
                        {
                            ub.Destination = displayCoordSystem.ChangeSystem(ump.UVCoord, CoordinateSystem.Cartesian) + displayCoordSystem.Origin;
                            ub.IsInsideBuilding = ump.IsInsideBuilding;
                        }
                }
                #endregion
                #region UnitKillPacket
                else if (pb is UnitKillPacket)
                {
                    UnitKillPacket ukp = (UnitKillPacket)pb;
                    unitList.RemoveAll(_ => _.HashCode == ukp.UnitHashCode);
                }
                #endregion
                #region UnitHPPacket
                else if (pb is UnitHPPacket)
                {
                    UnitHPPacket uhp = (UnitHPPacket)pb;
                    unitList.Find(_ => _.HashCode == uhp.UnitHashCode).HP = uhp.HP;
                }
                #endregion
                #region BuildingCreatePacket
                else if (pb is BuildingCreatePacket)
                {
                    BuildingCreatePacket bcp = (BuildingCreatePacket)pb;
                    Vector2 pos = displayCoordSystem.ChangeSystem(bcp.UVCoord, CoordinateSystem.Cartesian) + displayCoordSystem.Origin;

                    Building b = new Building(bcp.BuildingData, serverCoordSystem.UnitX.Length);
                    b.Position = pos;
                    b.HashCode = bcp.BuildingHashCode;

                    buildingList.Add(b);
                }
                #endregion
                #region BuildingHPPacket
                else if (pb is BuildingStatusPacket)
                {
                    BuildingStatusPacket bhp = (BuildingStatusPacket)pb;
                    if (buildingList.Exists(_ => _.HashCode == bhp.BuildingHashCode))
                    {
                        Building b = buildingList.Find(_ => _.HashCode == bhp.BuildingHashCode);

                        b.Owner = bhp.Owner;
                        b.HP = bhp.HP;
                        b.InsideUnitList = bhp.InsideHashCode.Select(_1 => unitList.Find(_2 => _2.HashCode == _1)).ToList();
                    }
                }
                #endregion
                #region TimePacket
                else if (pb is TimePacket)
                {
                    timeView.Remain = (Second)((TimePacket)pb).Remain;
                }
                #endregion
                #region GameEndPacket
                else if (pb is GameEndPacket)
                {
                    GameEndPacket gep = pb as GameEndPacket;
                    GameSystem.LogSystem.WriteLine("{0} {1}!", gep.Target, gep.State == GameEndPacket.PlayerState.Win ? "Won" : "Lose");
                    if (gep.Target == Me)
                    {
                        switch (gep.State)
                        {
                            case GameEndPacket.PlayerState.Win:
                                result.Texture = Texture.CreateFromBitmapPath("Resource/UI/win.png");
                                break;

                            case GameEndPacket.PlayerState.Lose:
                                result.Texture = Texture.CreateFromBitmapPath("Resource/UI/lose.png");
                                break;
                        }

                        result.Size = new Vector2(1, result.Texture.Size.Y / result.Texture.Size.X) * 400;
                        Finished = true;
                    }
                }
                #endregion
                else
                    GameSystem.LogSystem.WriteLine("Unknown Packet Handling: {0}-typed", pb.GetType().Name);
            }
            #endregion
                        
            #region Update Unit
            foreach (Unit ub in unitList)
                ub.Update(deltaTime);
            #endregion
            #region Update Building
            foreach (Building bd in buildingList)
                bd.Update(deltaTime);
            #endregion
            #region Card Use
            cardListView.Update(deltaTime);
            foreach (InputHelper.TouchData td in InputHelper.TouchList)
            {
                if (td.Action == Android.Views.MotionEventActions.Down)
                {
                    Vector2 duv = CoordinateSystem.Cartesian.ChangeSystem(td.Position - displayCoordSystem.Origin, displayCoordSystem);
                    Vector2 suv = CoordinateSystem.Cartesian.ChangeSystem(td.Position - serverCoordSystem.Origin, serverCoordSystem);
                    
                    serverConnector.EnqueueGamePacket(cardListView.UseCard(duv, suv, td.Position, buildingList, unitList, manaView));
                }
            }
            #endregion
            #region Unit Control
            serverConnector.EnqueueGamePacket(unitSelectView.CheckUnitMove(InputHelper.TouchList, buildingList, unitList, displayCoordSystem));
            #endregion
            #region Building Targeting
            foreach (InputHelper.TouchData td in InputHelper.TouchList)
            {
                if (td.Action == Android.Views.MotionEventActions.Down)
                {
                    float leftx = unitSelectView.Position.X - unitSelectView.Anchor.X * unitSelectView.Size.X;
                    float rightx = unitSelectView.Position.X + (1 - unitSelectView.Anchor.X) * unitSelectView.Size.X;
                    float downy = unitSelectView.Position.Y - unitSelectView.Anchor.Y * unitSelectView.Size.Y;
                    float upy = unitSelectView.Position.Y + (1 - unitSelectView.Anchor.Y) * unitSelectView.Size.Y;

                    if (leftx < td.Position.X && td.Position.X < rightx)
                        if (downy < td.Position.Y && td.Position.Y < upy)
                            continue;

                    Building b = buildingList.Find(_ => (_.Position - td.Position).Length < 30);
                    unitSelectView.TargetBuilding = b;
                }
            }
            #endregion

            #region Mana Update
            // Temporary Code
            if (buildingList.Find(_ => _.BuildingData.MaxHP == 500) != null)
            {
                if (buildingList.Find(_ => _.BuildingData.MaxHP == 500).Owner == Me)
                    manaView.Buffed = true;
                else
                    manaView.Buffed = false;
            }

            manaView.Update(deltaTime);
            #endregion

            unitSelectView.Update(deltaTime);
            timeView.Update(deltaTime);
        }
        public override void Render(Second deltaTime)
        {
            map.Render(ref ocam.Matrix);

            #region Render Building
            foreach (Building bd in buildingList)
                bd.Render(ref ocam.Matrix);
            #endregion
            #region Render Unit
            foreach (Unit ub in unitList)
                ub.Render(ref ocam.Matrix);
            #endregion

            #region Unit Movement (Touch-Drag-Drop)
            foreach (Unit u in clickedUnit)
            {
                Vector2 unitSize = displayCoordSystem.ChangeSystem(u.UnitData.UVSize, CoordinateSystem.Triangular);
                Sprite sp = new Sprite()
                {
                    Depth = RenderingConst.SelectBackDepth,
                    Size = new Vector2(1, 0.5f) * (unitSize.X + 25),
                    Texture = selectTexture,
                    Position = u.Position - unitSize.Y * 0.5f * Vector2.UnitY,
                };
                sp.Render(ref ocam.Matrix);
            }
            #endregion
            
            #region Render UIs
            timeView.Render(ref ocam.Matrix);
            cardListView.Render(ref ocam.Matrix);
            manaView.Render(ref ocam.Matrix);
            unitSelectView.Render(ref ocam.Matrix);
            #endregion

            if (Finished == true)
                result.Render(ref ocam.Matrix);
        }
    }
}
