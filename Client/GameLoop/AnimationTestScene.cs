﻿using P111.Client.GameObject;
using P111.GameObject.Unit;
using Tri;
using Tri.And;
using Tri.And.GameLoop;
using Tri.And.Rendering.Camera;

namespace P111.Client.GameLoop
{
    public class AnimationTestScene : GameLoopBase
    {
        Unit u;
        OrthographicCamera ocam;

        public AnimationTestScene()
            : base("AnimationTestScene")
        {
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            
            float scale = 2000;
            u = new Unit(UnitDataFactory.ApprenticeKnight, scale);
            u.Position = GameSystem.MainForm.Viewport / 2;
            u.Destination = u.Position;
            u.Color = Unit.ColorEnum.Red;

            ocam = new OrthographicCamera(GameSystem.MainForm.Viewport, GameSystem.MainForm.Viewport / 2);
        }
        public override void Update(Second deltaTime)
        {
            u.Update(deltaTime);
        }
        public override void Render(Second deltaTime)
        {
            u.Render(ref ocam.Matrix);
        }
    }
}
