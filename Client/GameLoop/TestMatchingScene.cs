﻿using P111.Client.GameObject;
using P111.Client.Network;
using P111.GameObject.Unit;
using P111.Network.Packet;
using P111.Network.Packet.Impl;
using P111.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using Tri;
using Tri.And;
using Tri.And.GameLoop;

namespace P111.Client.GameLoop
{
    public class TestMatchingScene : GameLoopBase
    {
        ServerConnector serverConnector;

        public TestMatchingScene()
            : base("_TestMatching")
        {
            User.Me = new User("Me");
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();

            FontManager.Initialize();
            UnitDataFactory.Initialize();

            GameSystem.LogSystem.WriteLine("Network Start");
            ServerConnector.Initialize(() =>
            {
                GameSystem.LogSystem.WriteLine("Network status: {0}", serverConnector.IsConnected);
                GameSystem.LogSystem.WriteLine("Sending Matching Packet");
                serverConnector.SendPacket(new MatchingStartPacket(User.Me));

                return null;
            });
            serverConnector = ServerConnector.Instance;
        }

        public override void Update(Second deltaTime)
        {
            foreach (PacketBase pb in serverConnector.GetPacketList(false))
            {
                if (pb is MatchingEndPacket)
                {
                    MatchingEndPacket mep = (MatchingEndPacket)pb;

                    List<Card> clist = new List<Card>();
                    clist.Add(CardFactory.ApprenticeKnight, 15);
                    clist.Add(CardFactory.ApprenticeArcher, 10);
                    clist.Add(CardFactory.ApprenticeMage, 10);

                    List<String> texList = new List<String>();
                    foreach (Card c in clist.Distinct())
                    {
                        String name = c.CardType.Name;
                        texList.Add(String.Format("Resource/Card/{0}_Card.png", name));
                        texList.Add(String.Format("Resource/Card/{0}_Desc.png", name));
                    }

                    texList.Add("Resource/UI/Mana_bar_gem.png");
                    texList.Add("Resource/UI/Mana_bar_background.png");
                    texList.Add("Resource/UI/Mana_bar_empty.png");
                    texList.Add("Resource/UI/Mana_bar_full.png");

                    texList.Add("Resource/UI/Time_background.png");
                    texList.Add("Resource/UI/Time_bar.png");

                    texList.Add("Resource/UI/Select.png");
                    texList.Add("Resource/UI/Map.png");

                    texList.Add("Resource/UI/lose.png");
                    texList.Add("Resource/UI/win.png");

                    texList.Add("Resource/Building/Castle.Red.png");
                    texList.Add("Resource/Building/Castle.Green.png");
                    texList.Add("Resource/Building/Castle.Blue.png");
                    texList.Add("Resource/Building/Tower.Red.png");
                    texList.Add("Resource/Building/Tower.Green.png");
                    texList.Add("Resource/Building/Tower.Blue.png");
                    texList.Add("Resource/Building/Tower.Colorless.png");

                    texList.Add("Resource/Unit/ApprenticeKnight/arm_left_sword.png");
                    texList.Add("Resource/Unit/ApprenticeKnight/arm_right.png");
                    texList.Add("Resource/Unit/ApprenticeKnight/body_lower.png");
                    texList.Add("Resource/Unit/ApprenticeKnight/body_upper.png");
                    texList.Add("Resource/Unit/ApprenticeKnight/head.png");
                    texList.Add("Resource/Unit/ApprenticeKnight/leg_left.png");
                    texList.Add("Resource/Unit/ApprenticeKnight/leg_right.png");

                    MainGameLoop testgl = new MainGameLoop(mep.UserList, clist);
                    ResourceLoadScene load = new ResourceLoadScene(texList, testgl);

                    GameSystem.RunGameLoop(load);
                }
            }
        }

        public override void Render(Second deltaTime)
        {
        }
    }
}

