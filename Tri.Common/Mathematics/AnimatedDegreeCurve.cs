﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Tri.Mathematics
{
    public enum InterpolationType
    {
        Linear,
        Quadratic,

        Custom,
    }

    public class AnimatedDegreeCurve
    {
        public delegate Tuple<float, float> WeightFunc(float weight);

        /// <summary>
        /// List of keyframe
        /// </summary>
        private List<KeyFrame<Degree>> keyFrame;

        private InterpolationType interType;
        private WeightFunc interFunc;

        private static Dictionary<InterpolationType, WeightFunc> interDic; 

        /// <summary>
        /// static .ctor
        /// </summary>
        static AnimatedDegreeCurve()
        {
            interDic = new Dictionary<InterpolationType, WeightFunc>();
            interDic.Add(InterpolationType.Linear, _ => new Tuple<float, float>(1 - _, _));
            interDic.Add(InterpolationType.Quadratic, _ => new Tuple<float, float>(1 - _ * _, _ * _));
        }

        /// <summary>
        /// .ctor
        /// </summary>
        public AnimatedDegreeCurve(InterpolationType interType)
        {
            keyFrame = new List<KeyFrame<Degree>>();
            this.interType = interType;

            Debug.Assert(interType != InterpolationType.Custom);
        }
        public AnimatedDegreeCurve(WeightFunc interFunc)
        {
            keyFrame = new List<KeyFrame<Degree>>();
            this.interType = InterpolationType.Custom;
            this.interFunc = interFunc;
        }

        /// <summary>
        /// Add keyframe
        /// </summary>
        /// <param name="time"></param>
        /// <param name="value"></param>
        public void AddKeyFrame(Second time, Degree value)
        {
            keyFrame.Add(new KeyFrame<Degree>(time, value));
            keyFrame.OrderBy(_ => _.Time);
        }

        /// <summary>
        /// Get next value of keyframe
        /// </summary>
        /// <returns></returns>
        public Degree GetValue(Second sec)
        {
            int index = 0;
            Second max = keyFrame.Last().Time;
            sec = sec - max * (float)Math.Floor(sec / max);

            foreach (KeyFrame<Degree> frame in keyFrame)
            {
                if (frame.Time > sec)
                {
                    index--;
                    break;
                }

                index++;
            }

            if (index == -1)
                return keyFrame[0].Value;
            if (index == keyFrame.Count)
                return keyFrame[keyFrame.Count - 1].Value;

            var curr = keyFrame[index];
            var next = keyFrame[index + 1];
            return GetInterpolatedValue(curr.Value, next.Value, (sec - curr.Time) / (next.Time - curr.Time));
        }

        /// <summary>
        /// Get interpolated value between lhs and rhs
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <param name="weight"></param>
        /// <returns></returns>
        private Degree GetInterpolatedValue(Degree lhs, Degree rhs, float weight)
        {
            float lweight, rweight;
            Tuple<float, float> temp;

            if (interDic.ContainsKey(interType))
                temp = interDic[interType].Invoke(weight);
            else
                temp = interFunc.Invoke(weight);
            Debug.Assert(temp != null);
            
            lweight = temp.Item1;
            rweight = temp.Item2;

            return lhs * lweight + rhs * rweight;
        }
    }
}
