﻿using OpenTK;
using System;
using System.Diagnostics;

namespace Tri.Physics.Collider
{
    [Obsolete("Collision Check routine is not properly implemented; may cause false negative error.")]
    public class CircleCollider : ColliderBase
    {
        /// <summary>
        /// Radius of circle
        /// </summary>
        public float Radius;
        
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="radius"></param>
        /// <param name="position"></param>
        public CircleCollider(float radius, Vector2 position)
            : base()
        {
            Radius = radius;
            Position = position;
        }

        /// <summary>
        /// Is this collider collided w/ other collider?
        /// </summary>
        /// <param name="op">Opposite collider</param>
        /// <returns></returns>
        public override Boolean IsCollided(ColliderBase op)
        {
            Debug.Assert(op != this);

            bool result = false;
            if (op is CircleCollider)
            {
                CircleCollider cc = op as CircleCollider;
                result = (this.Radius + cc.Radius) > (this.Position - cc.Position).Length;
            }
            else if (op is PolygonCollider)
            {
                PolygonCollider pc = op as PolygonCollider;
                result = pc.IsCollided(this);
            }
            else
                throw new ArgumentException();
            
            return result;
        }
    }
}
