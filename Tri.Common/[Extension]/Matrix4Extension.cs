﻿using OpenTK.Graphics;
using OpenTK;

namespace Tri
{
    public static class Matrix4Extension
    {
        public static float[] ToFloatArray(this Matrix4 m)        
        {
            float [] arr = new float [16];
            int i = 0;

            arr [i + 0] = m.Row0.X; arr [i + 1] = m.Row0.Y;
            arr [i + 2] = m.Row0.Z; arr [i + 3] = m.Row0.W;
            i += 4;

            arr [i + 0] = m.Row1.X; arr [i + 1] = m.Row1.Y;
            arr [i + 2] = m.Row1.Z; arr [i + 3] = m.Row1.W;
            i += 4;

            arr [i + 0] = m.Row2.X; arr [i + 1] = m.Row2.Y;
            arr [i + 2] = m.Row2.Z; arr [i + 3] = m.Row2.W;
            i += 4;

            arr [i + 0] = m.Row3.X; arr [i + 1] = m.Row3.Y;
            arr [i + 2] = m.Row3.Z; arr [i + 3] = m.Row3.W;

            return arr;
        }
    }
}

