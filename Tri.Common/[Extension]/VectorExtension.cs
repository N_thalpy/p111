﻿using Tri.Mathematics;
using OpenTK;

namespace Tri
{
    public static class VectorExtension
    {
        /// <summary>
        /// Rotate vector
        /// </summary>
        /// <param name="vec2">vector to rotate</param>
        /// <param name="angle">angle to rotate</param>
        /// <returns></returns>
        public static Vector2 Rotate(this Vector2 vec2, Degree angle)
        {
            float sin = Mathf.Sin(angle);
            float cos = Mathf.Cos(angle);

            return new Vector2(vec2.X * cos - vec2.Y * sin, vec2.X * sin + vec2.Y * cos);
        }

        public static Vector3 XY0 (this Vector2 vec2)
        {
            return new Vector3(vec2.X, vec2.Y, 0);
        }
    }
}
