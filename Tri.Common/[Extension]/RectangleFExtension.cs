﻿using OpenTK;
using System.Drawing;

namespace Tri
{
    public static class RectangleFExtension
    {
        public static Vector2 GetTopLeft(this RectangleF rect)
        {
            return new Vector2(rect.Left, rect.Top);
        }

        public static Vector2 GetTopRight(this RectangleF rect)
        {
            return new Vector2(rect.Right, rect.Top);
        }

        public static Vector2 GetBottomLeft(this RectangleF rect)
        {
            return new Vector2(rect.Left, rect.Bottom);
        }

        public static Vector2 GetBottomRight(this RectangleF rect)
        {
            return new Vector2(rect.Right, rect.Bottom);
        }
    }
}