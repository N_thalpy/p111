﻿using System;

namespace Tri
{
    public static class CastHelper
    {
        public static T Cast<T>(Object obj)
        {
            return (T)Convert.ChangeType(obj, typeof(T));
        }
    }
}

