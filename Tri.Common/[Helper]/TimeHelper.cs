﻿using System;
using System.Diagnostics;

namespace Tri
{
    public static class TimeHelper
    {
        public static Stopwatch sw;

        static TimeHelper()
        {
            sw = new Stopwatch();
        }

        public static void Tic()
        {
            sw.Reset();
            sw.Start();
        }
        public static void Toc(String str)
        {
            sw.Stop();
            Console.WriteLine("{0}: {1}ms Elapsed", str, sw.ElapsedMilliseconds);
        }
    }
}

