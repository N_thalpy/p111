﻿using System;
using System.Diagnostics;
using System.Text;
using Tri.SystemComponent;

namespace Tri.Crypto.Codec
{
    /// <summary>
    /// Codec which does nothing
    /// </summary>
    [UnitTest]
    public class NullCodec : CodecBase
    {
        /// <summary>
        /// .ctor
        /// </summary>
        public NullCodec()
        {
        }

        /// <summary>
        /// Returns original input array
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public override Byte[] Decode(Byte[] input)
        {
            return input;
        }
        /// <summary>
        /// Returns original input array
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public override Byte[] Encode(Byte[] input)
        {
            return input;
        }

        #region Unit Test
        public static void UnitTest()
        {
            String testString = "123123asdasd";
            NullCodec codec = new NullCodec();
            Byte[] input = Encoding.UTF8.GetBytes(testString);
            Byte[] encoded = codec.Encode(input);
            Byte[] decoded = codec.Decode(encoded);

            Debug.Assert(testString == Encoding.UTF8.GetString(decoded));
        }
        #endregion
    }
}
