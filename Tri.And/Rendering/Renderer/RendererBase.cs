﻿namespace Tri.And.Rendering.Renderer
{
    public abstract class RendererBase
    {
        protected enum RendererState
        {
            Idle,
            Drawing,
        };
        /// <summary>
        /// State of renderer
        /// </summary>
        protected RendererState state;

        /// <summary>
        /// .ctor
        /// </summary>
        protected RendererBase()
        {
            state = RendererState.Idle;
        }

        public abstract void Render(RenderParameter param);
    }
}
