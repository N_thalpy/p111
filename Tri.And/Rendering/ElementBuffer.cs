﻿using OpenTK.Graphics.ES30;
using System;
using System.Diagnostics;

namespace Tri.And.Rendering
{
    public class ElementBuffer : IDisposable
    {
        /// <summary>
        /// element buffer object handler
        /// </summary>
        private int ebo;
        /// <summary>
        /// Count of element in element buffer
        /// </summary>
        public int Count
        {
            get;
            private set;
        }

        #region .ctor
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="ebo"></param>
        /// <param name="count"></param>
        private ElementBuffer(int ebo, int count)
        {
            this.ebo = ebo;
            this.Count = count;
        }
        /// <summary>
        /// Create Element buffer
        /// </summary>
        /// <param name="elements"></param>
        /// <returns></returns>
        public static ElementBuffer Create(int[] elements)
        {
            int ebo;
            int count = elements.Length;
            GL.GenBuffers(1, out ebo);

            GLHelper.CheckGLError();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ebo);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(sizeof(int) * count), elements, BufferUsage.StaticDraw);

            int size;
            GL.GetBufferParameter(BufferTarget.ElementArrayBuffer, BufferParameterName.BufferSize, out size);
            if (count * sizeof(int) != size)
                throw new ApplicationException("Element data not uploaded correctly");

            return new ElementBuffer(ebo, count);
        }
        #endregion

        #region Bind/Unbind
        /// <summary>
        /// Bind ebo
        /// </summary>
        public void Bind()
        {
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ebo);
        }
        /// <summary>
        /// Unbind ebo
        /// </summary>
        public static void Unbind()
        {
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
        }
        #endregion

        #region Dispose
        /// <summary>
        /// Dispose element buffer
        /// </summary>
        public void Dispose()
        {
            Debug.Assert(ebo != 0);
            GL.DeleteBuffers(1, new int[]{ ebo });
            ebo = 0;
        }
        #endregion
    }
}
