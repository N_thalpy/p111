﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using Android.Graphics;
using OpenTK;
using OpenTK.Graphics.ES20;

namespace Tri.And.Rendering
{
    public class Texture : IDisposable
    {
        private static Object StaticLock;
        private static Dictionary<String, Texture> Caching;

        /// <summary>
        /// Handle for OpenTK
        /// </summary>
        public int TextureHandle
        {
            get;
            private set;
        }
        /// <summary>
        /// Original size of texture
        /// </summary>
        public Vector2 Size
        {
            get;
            private set;
        }
        
        /// <summary>
        /// static .ctor
        /// </summary>
        static Texture()
        {
            Caching = new Dictionary<String, Texture>();
            StaticLock = new object();
        }
        /// <summary>
        /// .ctor
        /// </summary>
        private Texture(int handle)
        {
            TextureHandle = handle;
        }

        /// <summary>
        /// Creates Texture from bitmap file
        /// </summary>
        /// <param name="fileName">Name of bitmap</param>
        /// <returns></returns>
        private static Texture CreateFromBitmap(Bitmap bitmap)
        {
            lock (Texture.StaticLock)
            {
                int id = GL.GenTexture();
                if (id == 0)
                    throw new InvalidOperationException();

                GL.BindTexture(TextureTarget.Texture2D, id);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);

                Android.Opengl.GLUtils.TexImage2D((int)All.Texture2D, 0, bitmap, 0);

                GL.GenerateMipmap(TextureTarget.Texture2D);

                Texture tex = new Texture(id);
                tex.Size = new Vector2(bitmap.Width, bitmap.Height);
                return tex;
            }
        }
        /// <summary>
        /// Creates Texture from bitmap file path
        /// </summary>
        /// <param name="fileName">Name of bitmap</param>
        /// <returns></returns>
        public static Texture CreateFromBitmapPath(String path)
        {
            if (Texture.Caching.ContainsKey(path))
                return Texture.Caching[path];
            
            if (GameSystem.Context.Assets.Exists(path) == false)
                throw new FileNotFoundException(String.Format("File {0} has not found", path));
            
            using (Stream s = GameSystem.Context.Assets.Open(path))
            {
                using (Bitmap bitmap = BitmapFactory.DecodeStream(s))
                {
                    Texture t = GameSystem.RunOnMainThread<Texture>(() =>
                    {
                        return CreateFromBitmap(bitmap);
                    });

                    lock (Texture.Caching)
                    {
                        Texture.Caching.Add(path, t);
                    }
                    return t;
                }
            }
        }
        /// <summary>
        /// Creates Texture from bitmap file path asyncronously.
        /// </summary>
        /// <returns>Texture</returns>
        /// <param name="path">Name of bitmap</param>
        public static Task<Texture> CreateFromBitmapPathAsync(String path, Action callback)
        {
            return Task.Run(() =>
            {
                Texture t = Texture.CreateFromBitmapPath(path);
                callback.Invoke();
                return t;
            });
        }
        /// <summary>
        /// Disposes texture
        /// </summary>
        public void Dispose()
        {
            if (TextureHandle != 0)
                GL.DeleteTexture(TextureHandle);
            else
                throw new InvalidOperationException("Texture already disposed.");
            
            TextureHandle = 0;
            Texture.Caching.Remove(Texture.Caching.Where(_ => _.Value.TextureHandle == TextureHandle).First().Key);
        }
    }
}
