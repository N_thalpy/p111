﻿using System;

namespace Tri.And.Rendering.VertexType
{
    public abstract class VertexTypeBase
    {
        public abstract void Bind();
    }
}

