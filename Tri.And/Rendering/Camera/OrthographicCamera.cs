﻿using OpenTK;
using System;

namespace Tri.And.Rendering.Camera
{
    /// <summary>
    /// Orthographic Camera.
    /// </summary>
    public class OrthographicCamera
    {
        public Matrix4 Matrix;
        
        /// <summary>
        /// Position of camera
        /// </summary>
        public Vector2 Center
        {
            get
            {
                return center;
            }
            set
            {
                if (value == center)
                    return;

                center = value;
                CalculateMatrix();
            }
        }
        private Vector2 center;
        /// <summary>
        /// Size of view
        /// </summary>
        public Vector2 Viewport
        {
            get
            {
                return viewport;
            }
            set
            {
                if (value == viewport)
                    return;

                viewport = value;
                CalculateMatrix();
            }
        }
        private Vector2 viewport;
        
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="viewport"></param>
        /// <param name="center"></param>
        public OrthographicCamera(Vector2 viewport, Vector2 center)
        {
            this.center = center;
            this.viewport = viewport;
            CalculateMatrix();
        }

        /// <summary>
        /// Move camera position
        /// </summary>
        private void CalculateMatrix()
        {
            Matrix4.CreateOrthographic(viewport.X, viewport.Y, -1f, 255f, out Matrix);
            Matrix4 view = Matrix4.CreateTranslation(new Vector3(-center.X, -center.Y, 0));
            Matrix4.Mult(ref view, ref Matrix, out Matrix);
        }

        public Vector3 ScreenToWorld(Vector2 screen, float z)
        {
            throw new NotImplementedException();
        }
        public Vector2 WorldToScreen(Vector3 world)
        {
            throw new NotImplementedException();
        }
    }
}
