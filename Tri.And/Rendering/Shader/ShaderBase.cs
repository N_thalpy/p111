﻿using OpenTK.Graphics.ES20;
using System;
using System.Diagnostics;
using System.Text;

namespace Tri.And.Rendering.Shader
{
    public abstract class ShaderBase : IDisposable
    {
        protected static int vShaderIndex = 0;

        protected int vertexShader;
        protected int fragmentShader;
        protected int shaderProgram;
        public int Program
        {
            get
            {
                return shaderProgram;
            }
        }

        protected ShaderBase(string vSource, string fSource)
        {
            CreateVertexShader(vSource);
            CreateFregmentShader(fSource);
            LinkShader();

            GLHelper.CheckGLError();
        }

        private void CreateVertexShader(string source)
        {
            vertexShader = GL.CreateShader(ShaderType.VertexShader);
            Debug.Assert(vertexShader != 0);
            CompileShader(vertexShader, source);
        }

        private void CreateFregmentShader(string source)
        {
            fragmentShader = GL.CreateShader(ShaderType.FragmentShader);
            Debug.Assert(fragmentShader != 0);
            CompileShader(fragmentShader, source);
        }

        private void CompileShader(int shader, string source)
        {
            int shaderStatusCode;

            GL.ShaderSource(shader, source);
            GL.CompileShader(shader);
            GL.GetShader(shader, ShaderParameter.CompileStatus, out shaderStatusCode);

            if (shaderStatusCode == 0)
            {
                int length = 0;
                GL.GetShader(shader, ShaderParameter.InfoLogLength, out length);
                if (length > 0)
                {
                    StringBuilder log = new StringBuilder(length);
                    GL.GetShaderInfoLog(shader, length, out length, log);
                    throw new InvalidOperationException("Couldn't compile shader: " + log.ToString());
                }
            }

            GLHelper.CheckGLError();
        }

        private void LinkShader()
        {
            shaderProgram = GL.CreateProgram();
            GL.AttachShader(shaderProgram, vertexShader);
            GL.AttachShader(shaderProgram, fragmentShader);

            GL.LinkProgram(shaderProgram);
        }

        public void Bind()
        {
            GL.UseProgram(shaderProgram);
        }

        public void Dispose()
        {
            if (vertexShader != 0) GL.DeleteShader(vertexShader); vertexShader = 0;
            if (fragmentShader != 0) GL.DeleteShader(fragmentShader); fragmentShader = 0;
            if (shaderProgram != 0) GL.DeleteProgram(shaderProgram); shaderProgram = 0;
        }
    }
}
