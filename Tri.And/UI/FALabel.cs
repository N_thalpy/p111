﻿using HAJE.VE.Rendering;
using OpenTK;
using System;
using System.Drawing;

namespace HAJE.VE.UI
{
    public class FALabel : View
    {
        /// <summary>
        /// Font to use
        /// </summary>
        public FontBasedAtlas FontBasedAtlas;
        
        /// <summary>
        /// Text to use
        /// </summary>
        public String Text;
        
        /// <summary>
        /// Color to use
        /// </summary>
        public Color Color;
        
        /// <summary>
        /// .ctor
        /// </summary>
        public FALabel()
            : base()
        {
            Text = String.Empty;
        }

        /// <summary>
        /// Draws texture
        /// </summary>
        protected override void OnRender()
        {
            Graphics g = Graphics.FromHwnd(GameSystem.MainForm.WindowInfo.Handle);
            SizeF stringSize = g.MeasureString(Text, FontBasedAtlas.Font);
            Vector2 size = new Vector2(stringSize.Width, stringSize.Height);

            Vector2 leftMost = new Vector2()
            {
                X = Position.X - size.X * Anchor.X,
                Y = Position.Y
            };

            Vector2 acc = leftMost;
            foreach (Char ch in Text)
            {
                Texture tex = FontBasedAtlas.GetTexture(ch);
                RenderHelper.Draw(acc, Anchor, tex.Size, Angle, Depth, tex, Color);

                acc.X += tex.Size.X;
            }
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public override void Dispose()
        {
        }
    }
}
