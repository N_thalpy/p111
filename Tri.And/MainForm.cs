﻿using System;
using Android.Content;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.ES20;
using OpenTK.Platform.Android;
using Tri.And.Rendering;

namespace Tri.And
{
    public class MainForm : AndroidGameView
    {
        public Vector2 Viewport
        {
            get;
            private set;
        }

        /// <summary>
        /// .ctor
        /// </summary>
        public MainForm(Context context)
            : base(context)
        {
            Viewport = new Vector2(Resources.DisplayMetrics.WidthPixels, Resources.DisplayMetrics.HeightPixels);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            Run();
        }

        protected override void CreateFrameBuffer()
        {
            ContextRenderingApi = GLVersion.ES2;
            base.CreateFrameBuffer();
        }

        /// <summary>
        /// Update Objects
        /// </summary>
        /// <param name="e"></param>
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);
            GameSystem.UpdateTick();
        }

        /// <summary>
        /// Renders Objects.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            GL.ClearColor(Color4.CornflowerBlue);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            
            GameSystem.RenderTick();

            SwapBuffers();
            GLHelper.CheckGLError();
        }
    }
}
