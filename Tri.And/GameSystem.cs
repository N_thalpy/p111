﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using Android.Content;
using OpenTK.Audio;
using Tri.And.GameLoop;
using Tri.And.Rendering.Camera;
using Tri.And.SystemComponent;
using Tri.Coroutine;
using Tri.SystemComponent;

namespace Tri.And
{
    /// Program.Main    -- calls --> GameSystem.Run
    /// GameSystem.Run  -- calls --> MainForm.Run (which overrides GameWindow.Run)
    /// MainForm.Run    -- calls --> GameSystem.UpdateTick & GameSystem.RenderTick
    public static class GameSystem
    {
        #region public get/private set variables
        public static TriVersion GameVersion
        {
            get;
            private set;
        }
        public static bool IsDebug;
        public static bool DoUnitTest;

        public static Context Context
        {
            get;
            private set;
        }
        public static MainForm MainForm
        {
            get;
            private set;
        }
        public static Profiler Profiler
        {
            get;
            private set;
        }
        public static LogSystem LogSystem
        {
            get;
            private set;
        }
        public static AudioContext AudioContext
        {
            get;
            private set;
        }
        public static UITracer UITracer
        {
            get;
            private set;
        }
        public static PolygonTracer PolygonTracer
        {
            get;
            private set;
        }
        public static CoroutineManager CoroutineManager
        {
            get;
            private set;
        }

        public static GameTime UpdateTime
        {
            get;
            private set;
        }
        public static GameTime RenderTime
        {
            get;
            private set;
        }

        public static OrthographicCamera DebugCamera
        {
            get;
            private set;
        }

        public static GameLoopBase CurrentGameLoop
        {
            get;
            private set;
        }
        #endregion
        private static bool isInitialized;

        private static Thread MainThread;
        private static Queue<Continuation> ContQueue;

        #region .ctor/init
        /// <summary>
        /// static .ctor
        /// </summary>
        static GameSystem()
        {
            GameVersion = TriVersion.CurrentVersion;
            DoUnitTest = true;

            ContQueue = new Queue<Continuation>();
        }
        /// <summary>
        /// Initializer
        /// </summary>
        public static void Initialize(Context context)
        {
            Context = context;
            MainThread = Thread.CurrentThread;

            UpdateTime = new GameTime();
            RenderTime = new GameTime();

            MainForm = new MainForm(context);
            Profiler = new Profiler();
            LogSystem = new LogSystem();
            AudioContext = new AudioContext();
            UITracer = new UITracer();
            PolygonTracer = new PolygonTracer();
            CoroutineManager = new CoroutineManager();

            DebugCamera = new OrthographicCamera(MainForm.Viewport, 0.5f * MainForm.Viewport);

            InputHelper.SetMainContext(MainForm);
        }
        #endregion

        #region Game related
        public static void LoadResources(Assembly asm)
        {
            foreach (Type t in asm.GetTypes())
            {
                var lr = t.GetMethod("LoadResource", new Type[] { });
                if (lr != null)
                    lr.Invoke(null, null);
            }
        }
        /// <summary>
        /// Assign Game Loop to loop
        /// </summary>
        /// <param name="gameLoopBase"></param>
        public static void RunGameLoop(GameLoopBase gameLoopBase)
        {
            CurrentGameLoop = gameLoopBase;
        }
        /// <summary>
        /// Run game.
        /// </summary>
        public static void Run()
        {
            // Initialize UpdateTime and RenderTime before enter the game
            UpdateTime.Refresh();
            RenderTime.Refresh();

            MainForm.Run();
        }
        /// <summary>
        /// Exit Game
        /// </summary>
        public static void Exit()
        {
            Environment.Exit(0);
        }
        #endregion

        public static void Initialize()
        {
            Profiler.Initialize();
            PolygonTracer.Initialize();

            isInitialized = true;
        }

        public static T RunOnMainThread<T>(Func<Object> func)
        {
            Continuation cont = new Continuation(func);
            if (Thread.CurrentThread == MainThread)
            {
                cont.Invoke();
            }
            else
            {
            lock (ContQueue)
                ContQueue.Enqueue(cont);
            while (cont.Finished == false)
                continue;
            }

            return (T)cont.Result;
        }

        #region Tick related
        /// <summary>
        /// Update game. Called for each update tick
        /// </summary>
        public static void UpdateTick()
        {
            Profiler.UpdateStart();

            if (isInitialized == false)
                Initialize();
            if (CurrentGameLoop.Status == GameLoopStatus.Created)
                CurrentGameLoop.Initialize();

            lock (ContQueue)
                while (ContQueue.Count != 0)
                {
                    Continuation a = ContQueue.Dequeue();
                    a.Invoke();
                }

            UpdateTime.Refresh();
            Second deltaTime = UpdateTime.DeltaTime;

            CoroutineManager.Update(deltaTime);
            UITracer.Update();
            PolygonTracer.Update();
            InputHelper.Update();

            CurrentGameLoop.Update(deltaTime);

            Profiler.UpdateEnd();
        }
        /// <summary>
        /// Render game. Called for each rendering tick
        /// </summary>
        public static void RenderTick()
        {
            Profiler.RenderStart();

            if (isInitialized == false)
                Initialize();
            if (CurrentGameLoop.Status == GameLoopStatus.Created)
                CurrentGameLoop.Initialize();

            Second deltaTime = RenderTime.DeltaTime;
            RenderTime.Refresh();

            CurrentGameLoop.Render(deltaTime);

            //Profiler.Render();
            //LogSystem.Render();

            //UITracer.Render();
            //PolygonTracer.Render();

            //Profiler.RenderEnd();
        }
        #endregion

        #region Dispose
        /// <summary>
        /// Dispose Game System
        /// </summary>
        public static void Dispose()
        {
            if (Profiler != null)
                Profiler.Dispose();
            if (LogSystem != null)
                LogSystem.Dispose();
            if (AudioContext != null)
                AudioContext.Dispose();
        }
        #endregion
    }
}
