﻿using OpenTK.Audio.OpenAL;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Tri.And.Sound
{
    public class SoundContext : IDisposable
    {
        #region private
        /// <summary>
        /// Handler for AL.GenSource
        /// </summary>
        private int sourceHandler;
        /// <summary>
        /// Handler for AL.GetBuffer
        /// </summary>
        private int bufferHandler;
        #endregion

        #region .ctor
        /// <summary>
        /// .ctor
        /// </summary>
        private SoundContext(int bufferHandler, int sourceHandler)
        {
            this.bufferHandler = bufferHandler;
            this.sourceHandler = sourceHandler;
        }

        public static SoundContext Create(String fileName)
        {
            int bufferHandler = AL.GenBuffer();
            int sourceHandler = AL.GenSource();

            if (GameSystem.Context.Assets.Exists(fileName) == false)
                throw new FileNotFoundException();

            int channels, bitRate, sampleRate;
            byte[] soundData;
            using (Stream s = GameSystem.Context.Assets.Open(fileName))
            {
                MemoryStream ms = new MemoryStream();
                s.CopyTo(ms);
                ms.Position = 0;
                soundData = LoadWave(ms, out channels, out bitRate, out sampleRate);
            
                ms.Dispose();
            }

            AL.BufferData(bufferHandler, GetSoundFormat(channels, bitRate), soundData, soundData.Length, sampleRate);
            AL.Source(sourceHandler, ALSourcei.Buffer, bufferHandler);

            return new SoundContext(bufferHandler, sourceHandler);
        }
        public static Task<SoundContext> CreateAsync(String fileName)
        {
            return Task.FromResult(SoundContext.Create(fileName));
        }
        #endregion

        #region Sound Control Related
        /// <summary>
        /// Set Gain Value.
        /// </summary>
        /// <param name="gain">Each multiplication by 2 equals +6dB amplification. Default value is 1</param>
        public void SetGain(float gain)
        {
            AL.Source(sourceHandler, ALSourcef.Gain, gain);
        }
        /// <summary>
        /// Enable or disable sound loop
        /// </summary>
        /// <param name="loop">loop if value is true</param>
        public void SetPlayback(bool loop)
        {
            AL.Source(sourceHandler, ALSourceb.Looping, loop);
        }
        /// <summary>
        /// Play sound of SoundContext
        /// </summary>
        public void Play()
        {
            AL.SourcePlay(sourceHandler);
        }
        /// <summary>
        /// Stop sound of SoundContext
        /// </summary>
        public void Stop()
        {
            AL.SourceStop(sourceHandler);
        }
        #endregion

        #region private Function
        /// <summary>
        /// Load wave data
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="channels"></param>
        /// <param name="bits"></param>
        /// <param name="rate"></param>
        /// <returns></returns>
        private static byte[] LoadWave(Stream stream, out int channels, out int bits, out int rate)
        {
#pragma warning disable CS0219
            if (stream == null)
                throw new ArgumentNullException("stream");

            using (BinaryReader reader = new BinaryReader(stream))
            {
                // RIFF header
                string signature = new string(reader.ReadChars(4));
                if (signature != "RIFF")
                    throw new NotSupportedException("Specified stream is not a wave file.");
                
                int riff_chunck_size = reader.ReadInt32();

                string format = new string(reader.ReadChars(4));
                if (format != "WAVE")
                    throw new NotSupportedException("Specified stream is not a wave file.");

                // WAVE header
                string format_signature = new string(reader.ReadChars(4));
                if (format_signature != "fmt ")
                    throw new NotSupportedException("Specified wave file is not supported.");

                int format_chunk_size = reader.ReadInt32();
                int audio_format = reader.ReadInt16();
                int num_channels = reader.ReadInt16();
                int sample_rate = reader.ReadInt32();
                int byte_rate = reader.ReadInt32();
                int block_align = reader.ReadInt16();
                int bits_per_sample = reader.ReadInt16();

                string list_signature = new string(reader.ReadChars(4));
                string data_signature = String.Empty;

                if (list_signature == "LIST")
                {
                    int list_size = reader.ReadInt32();
                    reader.BaseStream.Position += list_size;

                    data_signature = new string(reader.ReadChars(4));
                }
                else if (list_signature == "data")
                    data_signature = list_signature;


                if (data_signature != "data")
                    throw new NotSupportedException("Specified wave file is not supported.");
                int data_chunk_size = reader.ReadInt32();

                channels = num_channels;
                bits = bits_per_sample;
                rate = sample_rate;

                return reader.ReadBytes((int)reader.BaseStream.Length);
#pragma warning restore CS0219
            }
        }
        /// <summary>
        /// Get Format of sound
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="bits"></param>
        /// <returns></returns>
        private static ALFormat GetSoundFormat(int channels, int bits)
        {
            switch (channels)
            {
                case 1:
                    return bits == 8 ? ALFormat.Mono8 : ALFormat.Mono16;

                case 2:
                    return bits == 8 ? ALFormat.Stereo8 : ALFormat.Stereo16;

                default:
                    throw new NotSupportedException("The specified sound format is not supported.");
            }
        }
        #endregion

        /// <summary>
        /// Disposes audio context
        /// </summary>
        public void Dispose()
        {
            AL.DeleteSource(sourceHandler);
            AL.DeleteBuffer(bufferHandler);

            sourceHandler = 0;
            bufferHandler = 0;
        }
    }
}
