﻿using System;
using System.IO;
using Android.Content.Res;

namespace Tri.And
{
    public static class AssetManagerExtension
    {
        public static bool Exists(this AssetManager manager, String fileName)
        {
            String path = Path.GetDirectoryName(fileName);
            String name = Path.GetFileName(fileName);

            foreach (String s in manager.List(path))
                if (s == name)
                    return true;
            return false;
        }
    }
}

