﻿using System;
using System.Collections.Generic;
using System.Drawing;
using OpenTK;
using Tri.And.UI;
using Tri.And.UI.BitmapFont;

namespace Tri.And.SystemComponent
{
    public class UITracer
    {
        /// <summary>
        /// Dictionary of tracing views
        /// </summary>
        private Dictionary<View, BitmapLabel> tracerDic;
 
        /// <summary>
        /// .ctor
        /// </summary>
        public UITracer()
        {
            tracerDic = new Dictionary<View, BitmapLabel>();
        }

        /// <summary>
        /// Start to trace view
        /// </summary>
        /// <param name="view"></param>
        public void Trace(View view)
        {
            float leftwidth = -view.Anchor.X * view.Size.X;
            float upheight = -view.Anchor.Y * view.Size.Y;
            Vector2 pos = new Vector2(leftwidth, upheight).Rotate(view.Angle) + view.Position;
            
            BitmapLabel lb = new BitmapLabel()
            {
                Position = pos,
                Anchor = new Vector2(0, 1),
                Depth = 0,
                BitmapFont = BitmapFont.Create("SystemResources/arial.fnt"),
                Color = OpenTK.Graphics.Color4.White,
                Text = String.IsNullOrWhiteSpace(view.DebugName) ? view.GetType().Name.ToString() : view.DebugName,
            };
            tracerDic.Add(view, lb);
        }
        
        /// <summary>
        /// Update tracers
        /// </summary>
        public void Update()
        {
            foreach (View view in tracerDic.Keys)
            {
                float leftwidth = -view.Anchor.X * view.Size.X;
                float upheight = -(1 - view.Anchor.Y) * view.Size.Y;
                Vector2 pos = new Vector2(leftwidth, upheight).Rotate(view.Angle) + view.Position;
                tracerDic[view].Position = pos;

                String text = null;
                if (String.IsNullOrWhiteSpace(text))
                    text = view.GetType().Name.ToString();
                else
                    text = view.DebugName;
                tracerDic[view].Text = text;
            }
        }
        /// <summary>
        /// Render tracers
        /// </summary>
        public void Render()
        {
            if (GameSystem.IsDebug)
                foreach (View v in tracerDic.Keys)
                    tracerDic[v].Render(ref GameSystem.DebugCamera.Matrix);
        }
    }
}
