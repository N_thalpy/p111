﻿using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Tri.And.SystemComponent
{
    /// <summary>
    /// Game Timer
    /// </summary>
    public class GameTime
    {
        #region public Variables
        /// <summary>
        /// Delta time.
        /// </summary>
        public Second DeltaTime
        {
            get;
            private set;
        }
        /// <summary>
        /// Current frame count
        /// </summary>
        public long Frame
        {
            get;
            private set;
        }
        #endregion
        #region private Variables
        private Stopwatch sw;

        private long currentTick;
        private long oldTick;
        #endregion

        #region .ctor
        /// <summary>
        /// static .ctor
        /// </summary>
        static GameTime()
        {
        }
        /// <summary>
        /// .ctor
        /// </summary>
        public GameTime()
        {
            sw = new Stopwatch();
            sw.Start();

            currentTick = sw.ElapsedTicks;
            oldTick = currentTick;
            Frame = 0;
        }
        #endregion
        
        /// <summary>
        /// Reset GameTime
        /// </summary>
        public void Reset()
        {
            currentTick = sw.ElapsedTicks;
            oldTick = currentTick;
            Frame = 0;
        }
        /// <summary>
        /// Update GameTime
        /// </summary>
        public void Refresh()
        {
            oldTick = currentTick;
            currentTick = sw.ElapsedTicks;

            Debug.Assert(oldTick < currentTick);
            DeltaTime = (Second)((float)(currentTick - oldTick) / Stopwatch.Frequency);
            Frame++;
        }
    }
}
