﻿using OpenTK;
using P111.Player;
using P111.Server.AI.PacketHelper;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using P111.Network.Packet;

namespace P111.Server.AI.Impl
{
    public class NullAI : AIBase
    {
        public NullAI(User owner) 
            : base(owner)
        {
        }

        public override IEnumerable<PacketBase> Init(IEnumerable<User> ulist)
        {
            return new List<PacketBase>();
        }

        public override IEnumerator InnerStep()
        {
            while (true)
                yield return null;
        }
    }
}
