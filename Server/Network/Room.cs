﻿using Lidgren.Network;
using OpenTK;
using P111.GameObject.Building;
using P111.Network.Packet;
using P111.Network.Packet.Impl;
using P111.Player;
using P111.Server.Command;
using P111.Server.GameObject;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace P111.Server.Network
{
    /// <summary>
    /// Room class
    /// Update flow : .ctor -> repeat[ Step x 3 -> Update ]
    /// </summary>
    public class Room
    {
        private static int roomNumberCounter;

        // locked? true iff locked.
        private Dictionary<User, bool> lockDictionary;
        public bool Updatable
        {
            get
            {
                lock (lockDictionary)
                {
                    foreach (bool b in lockDictionary.Values)
                        if (b == true)
                            return false;

                    return true;
                }
            }
        }

        public int RoomNumber;

        public List<User> UserList;
        public List<Card> CardList;
        public List<Effect> EffectList;

        public Dictionary<User, NetConnection> SocketDic;
        public int HashCode;
       
        public List<Unit> UnitList;
        public List<Building> BuildingList;

        public List<int> LatencyList;
        public Stopwatch LatencyStopwatch;

        private int hashCounter;
        private int squadCounter;
        public int LpHashCounter;

        public float TimeRemain;

        public LockPacket PacketBackup;

        public Queue<PacketBase> PacketQueue;
        private Stopwatch sw;
        public Stopwatch ResendStopwatch;

        public bool Initialized;
        public RoomLogger Logger;

        #region .ctor / .dtor
        private Room(User a, User b, User c, NetConnection aSocket, NetConnection bSocket, NetConnection cSocket)
        {
            Logger = new RoomLogger();
            RoomNumber = Room.roomNumberCounter++;

            if (a.HashCode == b.HashCode || a.HashCode == c.HashCode || b.HashCode == c.HashCode)
                throw new ArgumentException(String.Format("Same hash codes! {0}, {1}, {2}", a, b, c));

            UserList = new List<User>(){ a, b, c };
            CardList = new List<Card>();
            EffectList = new List<Effect>();

            LatencyList = new List<int>();
            LatencyStopwatch = new Stopwatch();
            TimeRemain = 180; // TODO: Value Coupling

            SocketDic = new Dictionary<User, NetConnection>();
            SocketDic.Add(a, aSocket);
            SocketDic.Add(b, bSocket);
            SocketDic.Add(c, cSocket);

            lockDictionary = new Dictionary<User, bool>();
            lock (lockDictionary)
                foreach (KeyValuePair<User, NetConnection> kvp in SocketDic)
                    if (kvp.Value != null)
                        lockDictionary.Add(kvp.Key, false);

            UnitList = new List<Unit>();
            BuildingList = new List<Building>();

            sw = new Stopwatch();
            ResendStopwatch = new Stopwatch();
            ResendStopwatch.Start();
            PacketQueue = new Queue<PacketBase>();

            hashCounter = 1;
            squadCounter = 1;
            LpHashCounter = 1;

            Initialized = false;

            HashCode = a.HashCode + b.HashCode + c.HashCode;
        }

        // .ctor
        public static Room Create(User a, User b, User c, NetConnection aSocket, NetConnection bSocket, NetConnection cSocket)
        {
            Room r = new Room(a, b, c, aSocket, bSocket, cSocket);

            return r;
        }
        #endregion
        
        public void Step(StepPacket sp)
        {
            lock (lockDictionary)
            {
                if (lockDictionary[sp.Sender] == false)
                    return;

                lockDictionary[sp.Sender] = false;
            }

            lock (PacketQueue)
            {
                foreach (PacketBase pb in sp.Packets)
                    PacketQueue.Enqueue(pb);
            }
        }

        public LockPacket Update()
        {
            lock (lockDictionary)
            {
                foreach (User u in lockDictionary.Keys.ToList())
                    lockDictionary[u] = true;
            }

            lock (PacketQueue)
            {
                foreach (User u in UserList)
                    if (u.IsDummy == true)
                        foreach (PacketBase p in u.AI.FlushPacket())
                            PacketQueue.Enqueue(p);
            }
            Queue<PacketBase> outputPacketQueue = new Queue<PacketBase>();

            sw.Stop();
            lock (LatencyList)
                LatencyList.Add((int)sw.ElapsedMilliseconds);
            float deltaTime = sw.ElapsedMilliseconds / 1000f;
            sw.Reset();
            sw.Start();

            TimeRemain -= deltaTime;
            outputPacketQueue.Enqueue(new TimePacket(TimeRemain));

            if (Initialized == false)
            {
                Initialized = true;
                foreach (User u in UserList)
                    foreach (PacketBase pb in u.Init(UserList))
                       PacketQueue.Enqueue(pb);
                PacketQueue.Enqueue(new BuildingCreatePacket(null, BuildingDataFactory.Tent, Vector2.One / 3));
            }

            lock (PacketQueue)
            {
                #region Handling Packets
                while (PacketQueue.Count != 0)
                {
                    PacketBase pb = PacketQueue.Dequeue();
                    Debug.Assert(pb.GetType().IsDefined(typeof(SentByClientAttribute), false));

                    if (pb is UnitCreatePacket)
                    {
                        UnitCreatePacket ucp = (UnitCreatePacket)pb;
                        ucp.UnitHashCode = hashCounter++;
                        ucp.UnitSquadCode = squadCounter++;

                        outputPacketQueue.Enqueue(ucp);
                        Unit u = new Unit(ucp);
                        BuildingList.Find(_ => _.HashCode == ucp.TargetBuildingHash).InsideUnitList.Add(u);
                        UnitList.Add(u);
                    }
                    else if (pb is UnitCommandPacket)
                    {
                        UnitCommandPacket ucp = (UnitCommandPacket)pb;

                        foreach (Unit u in UnitList.FindAll(_ => _.HashCode == ucp.SquadHash))
                        {
                            Building b = BuildingList.Find(_ => _.InsideUnitList.Contains(u));
                            if (b != null && u.IsMovable(ucp.Command) == true)
                            {
                                b.InsideUnitList.Remove(u);
                                u.IsInsideBuilding = false;
                                u.SetDestination(ucp.Command);
                            }
                        }
                    }
                    else if (pb is BuildingCreatePacket)
                    {
                        BuildingCreatePacket bcp = (BuildingCreatePacket)pb;
                        bcp.BuildingHashCode = hashCounter++;

                        outputPacketQueue.Enqueue(bcp);
                        BuildingList.Add(new Building(bcp));
                    }
                    else if (pb is CardUsePacket)
                    {
                        CardUsePacket cup = (CardUsePacket)pb;
                        Card c = CardFactory.GetCard(cup.CardType);

                        CardList.Add(c);
                        c.Owner = cup.Sender;
                        c.OnUse?.Invoke(c, this, cup.UVPosition);
                    }
                    else
                    {
                        outputPacketQueue.Enqueue(pb);
                    }
                }
                #endregion
                PacketQueue.Clear();
            }

            // Remove Cards
            foreach (Card c in CardList)
                if (c.IsEffective == false)
                    c.OnRemove?.Invoke(c, this);
            CardList.RemoveAll(_ => _.IsEffective == false);

            // Card Update
            foreach (Card c in CardList)
                c.OnUpdate?.Invoke(c, this, deltaTime);
            
            foreach (Unit u in UnitList)
            {
                u.UpdateTimer(deltaTime);
                u.UpdateAggro(UnitList, BuildingList);
                u.UpdateAttack(UnitList, BuildingList);
                u.UpdateMove(deltaTime, BuildingList);
            }

            foreach (Building b in BuildingList)
            {
                b.UpdateTimer(deltaTime);
                b.UpdateAggro(UnitList, BuildingList);
                b.UpdateAttack(UnitList, BuildingList);
            }

            foreach (Unit u in UnitList)
                u.GetOutputPacket(ref outputPacketQueue);
            foreach (Building b in BuildingList)
                b.GetOutputPacket(UnitList, ref outputPacketQueue);

            UnitList.RemoveAll(_ => _.HP <= 0);

            foreach (PacketBase pb in outputPacketQueue)
                Debug.Assert(pb.GetType().IsDefined(typeof(SentByServerAttribute), false));
            
            while (true)
            {
                bool modified = false;

                List<User> removeCandidate = new List<User>();
                foreach (User u in UserList)
                    if (BuildingList.Exists(_ => _.Owner == u && _.BuildingData.TextureName == "Castle") == false)
                    {
                        outputPacketQueue.Enqueue(new GameEndPacket(u, GameEndPacket.PlayerState.Lose));
                        removeCandidate.Add(u);

                        UserList.Remove(u);
                        BuildingList.RemoveAll(_ => _.Owner == u);
                        UnitList.RemoveAll(_ => _.Owner == u);
                        modified = true;
                        break;
                    }

                UserList.RemoveAll(_ => removeCandidate.Contains(_));

                if (modified == false)
                    break;
            }
            if (UserList.Count == 1)
            {
                outputPacketQueue.Enqueue(new GameEndPacket(UserList[0], GameEndPacket.PlayerState.Win));
                UserList.RemoveAt(0);
            }

            Logger.WriteLog(PacketQueue, outputPacketQueue);
            return new LockPacket(LpHashCounter++, LatencyList.Count, outputPacketQueue.ToArray());
        }
    }
}

