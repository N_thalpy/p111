﻿using OpenTK;
using P111.GameObject.Unit;
using P111.Network;
using P111.Network.Packet;
using P111.Network.Packet.Impl;
using P111.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using static P111.Network.Packet.Impl.UnitCommandPacket;

namespace P111.Server.GameObject
{
    public enum UnitState
    {
        Idle,
        Move,
        Engage,
        MoveToEngage,
    }

    public class Unit : ObjectBase
    {
        private class Destination
        {
            public enum DestinationType
            {
                Unit,
                Building,
            }

            public readonly DestinationType TargetType;
            public ObjectBase TargteObject;
            public UnitCommandPacket.CommandEnum TargetPoint;

            public Destination(ObjectBase targetObject)
            {
                TargetType = DestinationType.Unit;
                TargteObject = targetObject;
            }
            public Destination(UnitCommandPacket.CommandEnum targetPoint)
            {
                TargetType = DestinationType.Building;
                TargetPoint = targetPoint;
            }

            public Vector2 GetTargetPosition()
            {
                switch (TargetType)
                {
                    case DestinationType.Building:
                        return UnitCommandPacket.CommandToPosition(TargetPoint);

                    case DestinationType.Unit:
                        return TargteObject.Position;

                    default:
                        throw new ArgumentException("Unknown Destination Type");
                }
            }
        }

        public static Dictionary<UnitCommandPacket.CommandEnum, Dictionary<UnitCommandPacket.CommandEnum, bool>> MovableDic;

        public int HashCode;
        public int SquadCode;

        public readonly User Owner;
        private UnitCommandPacket.CommandEnum CurrentCommand;
        private Stack<Destination> DestinationStack;
        private UnitState State;

        public ObjectBase TargetUnit;
        public UnitData UnitData;
        public List<Card> EffectedCard;

        public bool IsInsideBuilding;

        public float Timer;

        static Unit()
        {
            MovableDic = new Dictionary<UnitCommandPacket.CommandEnum, Dictionary<UnitCommandPacket.CommandEnum, bool>>();

            List<UnitCommandPacket.CommandEnum> enumList = new List<UnitCommandPacket.CommandEnum>();
            for (int idx = 1; idx < (int)UnitCommandPacket.CommandEnum.End; idx++)
                enumList.Add((UnitCommandPacket.CommandEnum)idx);

            foreach (UnitCommandPacket.CommandEnum e1 in enumList)
            {
                Dictionary<UnitCommandPacket.CommandEnum, bool> tmp = new Dictionary<UnitCommandPacket.CommandEnum, bool>();
                foreach (UnitCommandPacket.CommandEnum e2 in enumList)
                    tmp.Add(e2, false);
                MovableDic.Add(e1, tmp);
            }

            // Center
            MovableDic[CommandEnum.MoveToC1][CommandEnum.MoveToCenter] = true;
            MovableDic[CommandEnum.MoveToC2][CommandEnum.MoveToCenter] = true;
            MovableDic[CommandEnum.MoveToC3][CommandEnum.MoveToCenter] = true;

            MovableDic[CommandEnum.MoveToC1][CommandEnum.MoveToT12] = true;
            MovableDic[CommandEnum.MoveToT12][CommandEnum.MoveToT21] = true;
            MovableDic[CommandEnum.MoveToT21][CommandEnum.MoveToC2] = true;
            MovableDic[CommandEnum.MoveToC2][CommandEnum.MoveToT23] = true;
            MovableDic[CommandEnum.MoveToT23][CommandEnum.MoveToT32] = true;
            MovableDic[CommandEnum.MoveToT32][CommandEnum.MoveToC3] = true;
            MovableDic[CommandEnum.MoveToC3][CommandEnum.MoveToT31] = true;
            MovableDic[CommandEnum.MoveToT31][CommandEnum.MoveToT13] = true;
            MovableDic[CommandEnum.MoveToT13][CommandEnum.MoveToC1] = true;
        }
        public Unit(UnitCreatePacket ucp)
            : base()
        {
            UnitData unitData = UnitDataFactory.UnitDataDict[ucp.UnitDataHash];
            IsInsideBuilding = true;

            DestinationStack = new Stack<Destination>();
            HP = unitData.MaxHP;

            State = UnitState.Idle;

            Position = ucp.UVCoord;
            HashCode = ucp.UnitHashCode;
            SquadCode = ucp.UnitSquadCode;

            UnitData = unitData;
            Owner = ucp.Owner;

            List<UnitCommandPacket.CommandEnum> enumList = new List<UnitCommandPacket.CommandEnum>();
            for (int idx = 1; idx < (int)UnitCommandPacket.CommandEnum.End; idx++)
                enumList.Add((UnitCommandPacket.CommandEnum)idx);
            CurrentCommand = enumList.OrderBy(_ => (UnitCommandPacket.CommandToPosition(_) - ucp.UVCoord).Length).First();

            EffectedCard = new List<Card>();
        }

        public void UpdateTimer(float deltaTime)
        {
            Timer += deltaTime;
        }
        public void UpdateAggro(IEnumerable<Unit> unitList, IEnumerable<Building> buildingList)
        {
            // Disable Aggro Temporarily
            if (TargetUnit != null)
                if (TargetUnit.HP <= 0 ||
                    (TargetUnit is Unit && ((Unit)TargetUnit).IsInsideBuilding == true) ||
                    (TargetUnit.Position - Position).Length > UnitData.UVAggroRange)
                {
                    TargetUnit = null;
                    State = UnitState.Move;
                }

            while (DestinationStack.Count != 0)
            {
                if (DestinationStack.Peek().TargetType == Destination.DestinationType.Unit)
                    DestinationStack.Pop();
                else
                    break;
            }

            if (IsInsideBuilding == true)
                return;

            // TODO: Aggro Priority
            foreach (Unit target in unitList)
                if (TargetUnit == null)
                    if (Owner != target.Owner && target.IsInsideBuilding == false)
                        if ((Position - target.Position).Length < UnitData.UVAggroRange)
                        {
                            TargetUnit = target;
                            State = UnitState.MoveToEngage;
                        }
            foreach (Building target in buildingList)
                if (TargetUnit == null)
                    if (Owner != target.Owner && target.Disabled == false)
                        if ((Position - target.Position).Length < UnitData.UVAggroRange)
                        {
                            TargetUnit = target;
                            State = UnitState.MoveToEngage;
                        }

            if (TargetUnit != null)
                DestinationStack.Push(new Destination(TargetUnit));
        }
        public void UpdateAttack(List<Unit> unitList, List<Building> buildingList)
        {
            if (TargetUnit != null)
                if ((Position - TargetUnit.Position).Length < UnitData.UVAttackRange)
                {
                    State = UnitState.Engage;
                    if (Timer > UnitData.AttackCycle)
                    {
                        TargetUnit.HP -= UnitData.Damage;
                        Timer = 0;
                    }
                }
        }
        public void UpdateMove(float deltaTime, List<Building> buildingList)
        {
            switch (State)
            {
                case UnitState.Move:
                case UnitState.MoveToEngage:
                    if (deltaTime > 0)
                    {
                        Vector2 vel = (GetTargetPosition() - Position) / deltaTime;
                        float len = CoordinateSystem.Triangular.ChangeSystem(vel, CoordinateSystem.Cartesian).Length;
                        if (len > UnitData.UVSpeed)
                            vel = vel / len * UnitData.UVSpeed;

                        Position += vel * deltaTime;

                        // Save position before get aggro
                        if (DestinationStack.Count != 0 && Position == GetTargetPosition() && State == UnitState.Move)
                        {
                            Destination dest = DestinationStack.Peek();
                            if (dest.TargetType == Destination.DestinationType.Building)
                            {
                                Building b = buildingList.Find(_ => _.Position == dest.GetTargetPosition());
                                if (b != null)
                                {
                                    if (b.Owner == Owner && b.Disabled == false)
                                    {
                                        b.InsideUnitList.Add(this);
                                        IsInsideBuilding = true;
                                    }
                                    else if (b.Disabled == true)
                                    {
                                        b.HP = b.BuildingData.MaxHP;
                                        b.Owner = Owner;
                                        b.Disabled = false;
                                        b.InsideUnitList.Add(this);
                                        IsInsideBuilding = true;
                                    }
                                }

                                CurrentCommand = dest.TargetPoint;
                            }

                            DestinationStack.Pop();
                            State = UnitState.Idle;
                        }
                    }
                    break;
            }
        }

        public void GetOutputPacket(ref Queue<PacketBase> outputPacketQueue)
        {
            if (HP <= 0)
                HP = 0;

            if (HP <= 0)
            {
                outputPacketQueue.Enqueue(new UnitKillPacket(HashCode));
                return;
            }
            else
            {
                outputPacketQueue.Enqueue(new UnitHPPacket(HashCode, HP));
                outputPacketQueue.Enqueue(new UnitMovePacket(HashCode, GetTargetPosition(), Position, IsInsideBuilding));
            }
        }

        public bool IsMovable(UnitCommandPacket.CommandEnum targetPoint)
        {
            return MovableDic[targetPoint][CurrentCommand] || MovableDic[CurrentCommand][targetPoint];
        }
        public void SetDestination(UnitCommandPacket.CommandEnum targetPoint)
        {
            DestinationStack.Clear();
            DestinationStack.Push(new Destination(targetPoint));

            State = UnitState.Move;
        }
        public Vector2 GetTargetPosition()
        {
            if (DestinationStack.Count == 0)
                return Position;
            else
                return DestinationStack.Peek().GetTargetPosition();
        }
    }
}

