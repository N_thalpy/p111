﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using P111.GameObject.Card;
using P111.GameObject.Unit;
using P111.Network.Packet.Impl;

namespace P111.Server.GameObject
{
    public static class CardFactory
    {
        #pragma warning disable CS0414
        private static Card Heist;
        private static Card ApprenticeKnight;
        private static Card ApprenticeArcher;
        private static Card ApprenticeMage;
        private static Card Mage;
        private static Card Paladin;
        private static Card Knight;
        #pragma warning restore CS0414

        private static IEnumerable<Card> cardList;

        static CardFactory()
        {
            Heist = new Card()
            {
                CardType = CardType.Heist,
                OnUse = (c, r, v) =>
                {
                    c.ParamDic.Add("Timer", 5f);
                },
                OnUpdate = (c, r, dt) =>
                {
                    foreach (Unit u in r.UnitList.Where(_ => _.Owner == c.Owner))
                        if (u.EffectedCard.Contains(c) == false)
                        {
                            u.EffectedCard.Add(c);
                            u.UnitData.UVSpeed += 1;
                        }
                    c.ParamDic["Timer"] = (float)c.ParamDic["Timer"] - dt;
                    if ((float)c.ParamDic["Timer"] < 0)
                        c.OnRemove(c, r);
                },
                OnRemove = (c, r) =>
                {
                    foreach (Unit u in r.UnitList.Where(_ => _.Owner == c.Owner))
                        if (u.EffectedCard.Contains(c) == true)
                        {
                            u.EffectedCard.Remove(c);
                            u.UnitData.UVSpeed -= 1;
                        }
                },
            };
            ApprenticeKnight = CreateVanillaUnit(CardType.ApprenticeKnight, UnitDataFactory.ApprenticeKnight);
            ApprenticeArcher = CreateVanillaUnit(CardType.ApprenticeArcher, UnitDataFactory.ApprenticeArcher);
            ApprenticeMage = CreateVanillaUnit(CardType.ApprenticeMage, UnitDataFactory.ApprenticeMage);
            Mage = CreateVanillaUnit(CardType.Mage, UnitDataFactory.Mage);
            Knight = CreateVanillaUnit(CardType.Knight, UnitDataFactory.Knight);
            Paladin = CreateVanillaUnit(CardType.Paladin, UnitDataFactory.Paladin);

            cardList = typeof(CardFactory).GetFields(BindingFlags.NonPublic | BindingFlags.Static)
                          .Select(_ => _.GetValue(null))
                          .Where(_ => _.GetType() == typeof(Card))
                          .Select(_ => (Card)_);
        }

        private static Card CreateVanillaUnit(CardType ctype, UnitData ud)
        {
            return new Card()
            {
                CardType = ctype,
                OnUse = (c, r, v) =>
                {
                    c.IsEffective = false;

                    Building b = r.BuildingList.OrderBy(_ => (_.Position - v).Length).First();
                    r.PacketQueue.Enqueue(new UnitCreatePacket(c.Owner, ud.Hash, b.HashCode, b.Position));
                },
            };
        }

        public static Card GetCard(CardType cardType)
        {
            Card rv = cardList.Where(_ => _.CardType == cardType).FirstOrDefault();
            if (rv == null)
                throw new ArgumentException("Unknown Card Type");
            return (Card)rv.Clone();
        }
    }
}
