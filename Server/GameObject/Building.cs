﻿using P111.GameObject.Building;
using P111.Network.Packet;
using P111.Network.Packet.Impl;
using P111.Player;
using System.Collections.Generic;
using System.Linq;

namespace P111.Server.GameObject
{
    public class Building : ObjectBase
    {
        public int HashCode;

        public User Owner;
        public bool Disabled;

        public ObjectBase TargetUnit;
        public BuildingData BuildingData;

        public List<Unit> InsideUnitList;

        public float Timer;

        public Building(BuildingCreatePacket bcp)
            : base()
        {
            Disabled = false;
            HP = bcp.BuildingData.MaxHP;

            Position = bcp.UVCoord;
            HashCode = bcp.BuildingHashCode;

            BuildingData = bcp.BuildingData;
            InsideUnitList = new List<Unit>();

            Owner = bcp.Sender;
        }

        public void UpdateTimer(float deltaTime)
        {
            Timer += deltaTime;
        }
        public void UpdateAggro(IEnumerable<Unit> unitList, IEnumerable<Building> bulidingList)
        {
            // Disable Aggro Temporarily
            if (TargetUnit != null)
                if (TargetUnit.HP < 0 || (TargetUnit.Position - Position).Length > BuildingData.UVAttackRange)
                    TargetUnit = null;


            if (Disabled == false)
                foreach (Unit target in unitList)
                    if (TargetUnit == null)
                        if (Owner != target.Owner)
                            if ((Position - target.Position).Length < BuildingData.UVAttackRange)
                            {
                                TargetUnit = target;
                            }
        }
        public void UpdateAttack(List<Unit> unitList, List<Building> buildingList)
        {
            if (TargetUnit != null)
                if ((Position - TargetUnit.Position).Length < BuildingData.UVAttackRange)
                {
                    if (Timer > BuildingData.AttackCycle)
                    {
                        TargetUnit.HP -= BuildingData.Damage;
                        Timer = 0;
                    }
                }
        }

        public void GetOutputPacket(List<Unit> unitList, ref Queue<PacketBase> outputPacketQueue)
        {
            if (HP <= 0)
                HP = 0;

            if (HP <= 0)
            {
                Disabled = true;
                Owner = null;

                foreach (Unit u in InsideUnitList)
                    outputPacketQueue.Enqueue(new UnitKillPacket(u.HashCode));
                unitList.RemoveAll(_ => InsideUnitList.Contains(_));
                InsideUnitList.Clear();
                TargetUnit = null;
            }
            InsideUnitList.RemoveAll(_1 => unitList.Exists(_2 => _1 == _2) == false);

            outputPacketQueue.Enqueue(new BuildingStatusPacket(Owner, HashCode, HP, InsideUnitList.Select(_ => _.HashCode).ToArray()));        
        }
    }
}

