﻿using System;
using System.Collections.Generic;
using OpenTK;
using P111.GameObject.Card;
using P111.Player;
using P111.Server.Network;

namespace P111.Server.GameObject
{
    public delegate Unit Effect(Unit unit);
    public class Card : ICloneable
    {
        public CardType CardType;
        public User Owner;
        public bool IsEffective;

        public Action<Card, Room, Vector2> OnUse;
        public Action<Card, Room, float> OnUpdate;
        public Action<Card, Room> OnRemove;

        public Dictionary<String, Object> ParamDic;

        public Card()
        {
            IsEffective = true;
            ParamDic = new Dictionary<string, object>();
        }

        public object Clone()
        {
            return new Card()
            {
                CardType = CardType,
                IsEffective = IsEffective,
                OnUse = OnUse,
                OnUpdate = OnUpdate,
                OnRemove = OnRemove,
            };
        }
    }
}
