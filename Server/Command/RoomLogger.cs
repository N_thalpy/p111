﻿using P111.Network.Packet;
using P111.Player;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using P111.GameObject.Card;

namespace P111.Server.Command
{
    public class RoomLogger
    {
        [Serializable]
        public class LogData
        {
            public int Frame;
            public List<PacketBase> InputPacket;
            public List<PacketBase> OutputPacket;

            public LogData()
            {
                Frame = -1;
                InputPacket = new List<PacketBase>();
                OutputPacket = new List<PacketBase>();
            }
            public LogData(int frame, IEnumerable<PacketBase> inputPacket, IEnumerable<PacketBase> outputPacket)
            {
                Frame = frame;
                InputPacket = inputPacket.ToList();
                OutputPacket = outputPacket.ToList();
            }
        }

        private static XmlSerializer xs;

        private int currentFrame;
        private List<LogData> logList;
     
        static RoomLogger()
        {
            xs = new XmlSerializer(typeof(List<LogData>), 
                typeof(RoomLogger).Assembly.GetTypes()
                .Where(_ => _.BaseType == typeof(PacketBase))
                                   .Concat(new Type[] { typeof(User), typeof(CardType) })
                .ToArray());
        }   
        public RoomLogger()
        {
            currentFrame = 0;
            logList = new List<LogData>();
        }

        public void WriteLog(IEnumerable<PacketBase> inputPacket, IEnumerable<PacketBase> outputPacket)
        {
            currentFrame++;
            lock (logList)
                logList.Add(new LogData(currentFrame, inputPacket, outputPacket));
        }
        public void Dump(Stream s)
        {
            lock (logList)
                xs.Serialize(s, logList);
        }
    }
}
