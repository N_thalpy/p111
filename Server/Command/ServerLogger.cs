﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace P111.Server.Command
{
    public enum ServerLogType
    {
        Misc,
        Network,
        Command,
        Unknown,
    }

    public class ServerLogger
    {
        [Serializable]
        public class LogData
        {
            public ServerLogType Type;
            public String Log;

            public LogData()
            {
                Type = ServerLogType.Unknown;
                Log = String.Empty;
            }
            public LogData(ServerLogType type, String log)
            {
                Type = type;
                Log = log;
            }
            public override string ToString()
            {
                String prefix = String.Empty;
                switch (Type)
                {
                    case ServerLogType.Misc:
                        prefix = "MISC";
                        break;

                    case ServerLogType.Command:
                        prefix = "CMD ";
                        break;

                    case ServerLogType.Network:
                        prefix = "NET ";
                        break;

                    default:
                        throw new ArgumentException("Unknown Log Type");
                }

                return String.Format("{0} {1}", prefix, Log.Trim());
            }
        }

        private static XmlSerializer xs;
        private List<LogData> logList;

        static ServerLogger()
        {
            xs = new XmlSerializer(typeof(List<LogData>));
        }
        public ServerLogger()
        {
            logList = new List<LogData>();
        }
        
        public void WriteLog(ServerLogType logType, String format, params Object[] args)
        {
            lock (logList)
                logList.Add(new LogData(logType, String.Format(format, args)));
        }
        public void Dump(Stream s)
        {
            lock (logList)
                xs.Serialize(s, logList);
        }

        public IEnumerable<String> GetLogs(int maxCount)
        {
            lock (logList)
            {
                List<String> stringList = new List<String>();

                int offset = Math.Max(0, logList.Count - maxCount);
                for (int i = 0; i < Math.Min(maxCount, logList.Count); i++)
                    stringList.Add(logList[offset + i].ToString());

                return stringList;
            }
        }
    }
}
