﻿using P111.Player;
using P111.Server.Network;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace P111.Server.Command
{
    public class CommandExecuter
    {
        public static List<CommandExecuter> ExecuterList;
        
        public delegate bool Checker(String command);
        public delegate void Executer(String command);

        public Checker Check;
        public Executer Execute;

        static CommandExecuter()
        {
            ExecuterList = new List<CommandExecuter>();

            #region CDM; Create Dummy
            ExecuterList.Add(new CommandExecuter(
                (_) => _ == "cdm",
                (_) =>
                {
                    Program.SendDummyMatching();
                    Program.Logger.WriteLog(ServerLogType.Command, "Dummy Player Generated");
                }));
            #endregion
            #region DIS; Disconnect
            ExecuterList.Add(new CommandExecuter(
                (_) =>
                {
                    String[] splitted = _.Split(' ');

                    int temp;
                    if (splitted.Length == 2)
                        if (splitted[0] == "dis" && Int32.TryParse(splitted[1], out temp) == true)
                            return true;
                    return false;
                },
                (_) =>
                {
                    Int32 hash = Int32.Parse(_.Split(' ')[1]);
                    if (Program.RemoveUser(hash) == true)
                        Program.Logger.WriteLog(ServerLogType.Command, "Player {0} Removed", hash);
                    else
                        Program.Logger.WriteLog(ServerLogType.Command, "Unable to Find Player {0}", hash);
                }));
            #endregion
            #region DUMP; Dump log
            ExecuterList.Add(new CommandExecuter(
                (_) =>
                {
                    String[] splitted = _.Split(' ');
                    if (splitted.Length == 0 || splitted[0] != "dump")
                        return false;

                    if (splitted.Length == 3 && splitted[1] == "c")
                        return true;

                    int temp;
                    if (splitted.Length == 4 && splitted[1] == "r" && Int32.TryParse(splitted[2], out temp) == true)
                        return true;

                    return false;
                },
                (_) =>
                {
                    String[] splitted = _.Split(' ');
                    if (splitted[1] == "c")
                    {
                        try
                        {
                            using (StreamWriter sw = new StreamWriter(splitted[2]))
                                Program.Logger.Dump(sw.BaseStream);
                            Program.Logger.WriteLog(ServerLogType.Command, "Server Log Dumped to {0}", splitted[2]);
                        }
                        catch (Exception e)
                        {
                            Program.Logger.WriteLog(ServerLogType.Command, e.ToString());
                        }
                    }
                    else if (splitted[1] == "r")
                    {
                        int roomNumber = Int32.Parse(splitted[2]);
                        Room r = Program.RoomDic.Values.Where(__ => __.RoomNumber == roomNumber).FirstOrDefault();

                        if (r == null)
                            Program.Logger.WriteLog(ServerLogType.Command, "Unable to Find Room #{0}", roomNumber);
                        else
                        {
                            try
                            {
                                using (StreamWriter sw = new StreamWriter(splitted[3]))
                                    r.Logger.Dump(sw.BaseStream);
                                Program.Logger.WriteLog(ServerLogType.Command, "Room #{0} Log Dumped to {1}", roomNumber, splitted[3]);
                            }
                            catch (Exception e)
                            {
                                Program.Logger.WriteLog(ServerLogType.Command, e.ToString());
                            }
                        }
                    }
                }));
            #endregion
            #region EXIT; Exit Server
            ExecuterList.Add(new CommandExecuter(
                (_) => _ == "exit",
                (_) =>
                {
                    Environment.Exit(0);
                }));
            #endregion
            #region AQ; Auto Queue
            ExecuterList.Add(new CommandExecuter(
                (_) => _ == "aq",
                (_) =>
                {
                    ServerConfig.EnableAutoQueue ^= true;
                    Program.Logger.WriteLog(ServerLogType.Command, "Autoqueue has {0}.",
                        ServerConfig.EnableAutoQueue == true ? "enabled" : "disabled");
                }));
            #endregion
        }
        private CommandExecuter(Checker check, Executer exec)
        {
            Check = check;
            Execute = exec;
        }
    }
}