﻿using Lidgren.Network;
using P111.Network;
using P111.Network.Packet;
using P111.Network.Packet.Impl;
using P111.Player;
using P111.Server.AI.Impl;
using P111.Server.Command;
using P111.Server.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Xml.Serialization;
using P111.GameObject.Building;
using OpenTK;
using P111.GameObject.Unit;

namespace P111.Server
{
    public static class Program
    {
        public static ServerLogger Logger;
        public static IReadOnlyList<Room> RoomList
        {
            get
            {
                return roomList.AsReadOnly();
            }
        }
        public static IReadOnlyDictionary<User, Room> RoomDic
        {
            get
            {
                return roomDic;
            }
        }

        static NetServer server;

        static Dictionary<NetConnection, User> userDic;
        static Dictionary<User, NetConnection> netConnectionDic;
        static List<Room> roomList;
        static Dictionary<User, Room> roomDic;

        static Queue<User> matchingQueue;

        static String commandLine;
        static List<CommandExecuter> executerList;

        [STAThread]
        static void Main()
        {
            UnitDataFactory.Initialize();

            // Initializing
            Console.Title = "Project. 111 Server";
            Console.CursorVisible = false;

            try
            {
                Console.WindowWidth = Console.LargestWindowWidth - 10;
                Console.WindowHeight = Console.LargestWindowHeight - 5;
            }
            catch (NotSupportedException)
            {
            }

            Console.Clear();

            Logger = new ServerLogger();
            
            userDic = new Dictionary<NetConnection, User>();
            netConnectionDic = new Dictionary<User, NetConnection>();
            roomList = new List<Room>();
            roomDic = new Dictionary<User, Room>();
            matchingQueue = new Queue<User>();

            commandLine = String.Empty;
            executerList = CommandExecuter.ExecuterList;
                        
            // Setup Network
            NetPeerConfiguration config = new NetPeerConfiguration("P111");
            config.MaximumConnections = 100;
            config.Port = NetworkConstant.Port;

            server = new NetServer(config);
            server.Start();

            // Enable Networking Thread
            new Thread(() =>
            {
                while (true)
                {
                    Update();
                    Thread.Sleep(1000 / 60); // 1 Frame
                }
            }).Start();

            // Enable Render Thread
            new Thread(() =>
            {
                while (true)
                {
                    Render();
                    Thread.Sleep(1000);
                }
            }).Start();

            ServerConfig.EnableAutoQueue = true;

            // Get Command Input
            while (true)
            {
                ConsoleKeyInfo k = Console.ReadKey(true);
                if (k.Key == ConsoleKey.Enter)
                {
                    bool found = false;
                    foreach (CommandExecuter exec in executerList)
                        if (exec.Check(commandLine) == true)
                        {
                            found = true;
                            exec.Execute(commandLine);
                            break;
                        }

                    if (found == false)
                        Program.Logger.WriteLog(ServerLogType.Command, "Command {0} Not Found", commandLine);
                    commandLine = String.Empty;
                }
                else if (k.Key == ConsoleKey.Backspace)
                {
                    if (commandLine.Length > 0)
                        commandLine = commandLine.Substring(0, commandLine.Length - 1);
                }
                else
                    commandLine += k.KeyChar;

                Render();
            }
        }

        #region Helpers for CommandExecuter
        public static void SendDummyMatching()
        {
            User u = null;
            do
            {
                u = new User("DUMMY");
                u.IsDummy = true;
                u.AI = new NullAI(u);
            } while (netConnectionDic.Keys.ToList().Exists(_ => _.HashCode == u.HashCode));

            lock (netConnectionDic)
                netConnectionDic.Add(u, null);
            lock (matchingQueue)
                matchingQueue.Enqueue(u);
        }
        public static bool RemoveUser(int hash)
        {
            if (netConnectionDic.Keys.ToList().Exists(_ => _.HashCode == hash) == false)
                return false;

            User target = netConnectionDic.Keys.Where(_ => _.HashCode == hash).First();
            lock (netConnectionDic)
                netConnectionDic.Remove(target);
            lock (matchingQueue)
                if (matchingQueue.Contains(target))
                {
                    Queue<User> backup = new Queue<User>();
                    do
                    {
                        User u = matchingQueue.Dequeue();
                        if (u != target)
                            backup.Enqueue(u);
                    } while (matchingQueue.Count != 0);
                    do
                    {
                        matchingQueue.Enqueue(backup.Dequeue());
                    } while (backup.Count != 0);
                }

            if (Program.roomList.Exists(_ => _.UserList.Contains(target)))
                Program.DisposeRoom(Program.RoomList.Where(_ => _.UserList.Contains(target)).First());

            return true;
        }

        public static void DisposeRoom(Room r)
        {
            roomList.Remove(r);
            IEnumerable<User> removeCandidate = roomDic.Where(_ => _.Value == r).Select(_ => _.Key);
            foreach (User u in removeCandidate)
                roomDic.Remove(u);
        }
        #endregion

        #region Update/Render
        private static void Update()
        {
            NetIncomingMessage im;
            Queue<NetIncomingMessage> messageQueue = new Queue<NetIncomingMessage>();
            do
            {
                im = server.ReadMessage();
                if (im != null)
                    messageQueue.Enqueue(im);
            } while (im != null);

            while (messageQueue.Count != 0)
            {
                im = messageQueue.Dequeue();

                // handle incoming message
                switch (im.MessageType)
                {
                    case NetIncomingMessageType.DebugMessage:
                    case NetIncomingMessageType.ErrorMessage:
                    case NetIncomingMessageType.WarningMessage:
                    case NetIncomingMessageType.VerboseDebugMessage:
                        Program.Logger.WriteLog(ServerLogType.Network, im.ReadString());
                        break;

                    case NetIncomingMessageType.StatusChanged:
                        NetConnectionStatus status = (NetConnectionStatus)im.ReadByte();
                        im.ReadString();
                        if (status == NetConnectionStatus.Connected)
                        {
                            XmlSerializer userXs = new XmlSerializer(typeof(User));
                            User user = userXs.Deserialize<User>(im.SenderConnection.RemoteHailMessage.ReadString());

                            Program.Logger.WriteLog(ServerLogType.Network, "New Connection From " + user);
                            userDic.Add(im.SenderConnection, user);
                            netConnectionDic.Add(user, im.SenderConnection);
                        }
                        break;

                    case NetIncomingMessageType.Data:
                        // incoming chat message from a client
                        int length = im.ReadInt32();
                        Byte[] bytes = im.ReadBytes(length);

                        if (PacketSerializer.TryDeserialize<StepPacket>(bytes))
                        {
                            StepPacket pb = PacketSerializer.Deserialize<StepPacket>(bytes);
                            if (RoomDic.ContainsKey(pb.Sender))
                                if (pb.Frame == RoomDic[pb.Sender].LatencyList.Count)
                                    Program.RoomDic[pb.Sender].Step(pb);
                        }
                        else if (PacketSerializer.TryDeserialize<MatchingStartPacket>(bytes))
                        {
                            MatchingStartPacket pb = PacketSerializer.Deserialize<MatchingStartPacket>(bytes);
                            lock (matchingQueue)
                                matchingQueue.Enqueue(pb.User);

                            if (ServerConfig.EnableAutoQueue == true)
                            {
                                SendDummyMatching();
                                SendDummyMatching();
                            }
                        
                            Program.Logger.WriteLog(ServerLogType.Network, "Matching Start Received");
                        }
                        else
                            throw new ArgumentException(String.Format("Unknown Packet Received; {0}", new String(bytes.Select(_ => (char)_).ToArray())));

                        break;

                    default:
                        Program.Logger.WriteLog(ServerLogType.Network, "Unhandled type: {0} {1} bytes {2} | {3}",
                            im.MessageType, im.LengthBytes, im.DeliveryMethod, im.SequenceChannel);
                        break;
                }
                server.Recycle(im);
            }
            
            #region Matching
            lock (matchingQueue)
                if (matchingQueue.Count >= 3)
                {
                    User a, b, c;
                    a = matchingQueue.Dequeue();
                    b = matchingQueue.Dequeue();
                    c = matchingQueue.Dequeue();
                    
                    Program.Logger.WriteLog(ServerLogType.Network, "Room has generated w/ {0}, {1}, and {2}", a, b, c);

                    User[] ulist = new User[] { a, b, c };
                    PacketBase pb = new MatchingEndPacket(ulist);
                    foreach (User u in ulist)
                    {
                        if (u.IsDummy == false)
                        {
                            NetOutgoingMessage msg = server.CreateMessage();
                            Byte[] arr = PacketSerializer.Serialize(pb);
                            msg.Write(arr.Length);
                            msg.Write(arr);

                            Program.Logger.WriteLog(ServerLogType.Network, "Sent MEP");
                            netConnectionDic[u].SendMessage(msg, NetDeliveryMethod.ReliableOrdered, 0);
                        }
                   }

                    Room r = Room.Create(a, b, c, netConnectionDic[a], netConnectionDic[b], netConnectionDic[c]);
                    roomDic.Add(a, r);
                    roomDic.Add(b, r);
                    roomDic.Add(c, r);
                }
            #endregion
            #region Update Rooms
            try
            {
                foreach (Room r in Program.RoomDic.Values)
                {
                    if (r.Updatable == true)
                    {
                        r.ResendStopwatch.Restart();

                        LockPacket lp = r.Update();
                        r.PacketBackup = lp;
                        foreach (NetConnection socket in r.SocketDic.Values)
                        {
                            if (socket != null)
                            {
                                NetOutgoingMessage msg = server.CreateMessage();
                                Byte[] arr = PacketSerializer.Serialize(lp);
                                msg.Write(arr.Length);
                                msg.Write(arr);

                                socket.SendMessage(msg, NetDeliveryMethod.ReliableOrdered, 0);
                            }
                        }
                    }
                    else if (r.ResendStopwatch.ElapsedMilliseconds > 100)
                    {
                        r.ResendStopwatch.Restart();

                        foreach (NetConnection socket in r.SocketDic.Values)
                        {
                            if (socket != null)
                            {
                                NetOutgoingMessage msg = server.CreateMessage();
                                Byte[] arr = PacketSerializer.Serialize(r.PacketBackup);
                                msg.Write(arr.Length);
                                msg.Write(arr);

                                socket.SendMessage(msg, NetDeliveryMethod.ReliableOrdered, 0);
                            }
                        }
                    }
                }

                List<User> removeCandidate = roomDic.Keys.Where(_ => roomDic[_].UserList.Contains(_) == false).ToList();
                foreach (User u in removeCandidate)
                    roomDic.Remove(u);
            }
            catch (Exception e)
            {
                Logger.WriteLog(ServerLogType.Misc, e.ToString());
            }

            #endregion
        }
        private static void Render()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Clear();

            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Project. 111 Server\n");
            Console.ForegroundColor = ConsoleColor.White;

            // Write Connection List
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine("Connection List:");

            Console.BackgroundColor = ConsoleColor.Blue;

            lock (server.Connections)
                foreach (User u in netConnectionDic.Keys)
                {
                    String status = String.Empty;
                    String roomNo = "-";
                    String latency = "";

                    if (Program.RoomDic.Keys.Contains(u))
                    {
                        status = "ING";
                        roomNo = Program.RoomDic[u].RoomNumber.ToString();

                        lock (Program.roomDic[u].LatencyList)
                            if (Program.roomDic[u].LatencyList.Count != 0)
                            {   
                                List<int> llist = Program.roomDic[u].LatencyList;
                                int avg = (int)llist.Average();
                                int dev = (int)Math.Sqrt(llist.Select(_ => (_ - avg) * (_ - avg)).Average());
                                latency = String.Format("Lt: μ{0}ms/{1}fps σ{2}ms F: {3}", avg, avg == 0 ? "NaN" : (1000/avg).ToString(), dev, llist.Count);
                            }
                    }
                    else if (matchingQueue.Contains(u))
                        status = "QUE";
                    else
                        status = "IDL";

                    Console.WriteLine("\t{0,-30} {1,4} {2,3}  | {3}", u, status, roomNo, latency);
                }

            // Write Messages
            Console.SetCursorPosition(0, 20);
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine("Logs:");

            Console.BackgroundColor = ConsoleColor.Blue;
            foreach (String s in Logger.GetLogs(10))
                Console.WriteLine(s);            

            #region Render Command Line
            Console.BackgroundColor = ConsoleColor.Black;
            Console.SetCursorPosition(0, Console.WindowHeight - 3);
            Console.WriteLine();
            Console.WriteLine(String.Format("> {0}", commandLine));
            #endregion
        }
        #endregion
    }
}
